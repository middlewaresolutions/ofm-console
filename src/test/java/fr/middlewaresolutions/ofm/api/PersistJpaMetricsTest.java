/**
 * 
 */
package fr.middlewaresolutions.ofm.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;

import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.core.InfrastructureBean;
import fr.middlewaresolutions.ofmconsole.core.ObjectNameFactory;
import fr.middlewaresolutions.ofmconsole.core.jpa.PersistJPAMetricsBean;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.NumericValue;
import fr.middlewaresolutions.ofmconsole.metric.TextValue;
import fr.middlewaresolutions.ofmconsole.model.Application;
import fr.middlewaresolutions.ofmconsole.model.Base;
import fr.middlewaresolutions.ofmconsole.model.JVM;
import fr.middlewaresolutions.ofmconsole.model.OSBResource;
import fr.middlewaresolutions.ofmconsole.performance.PerformanceTrace;

/**
 * Tests for JPA Persistence
 * @author Emmanuel
 *
 */
public class PersistJpaMetricsTest extends AbstractTest {

	@EJB
	private Infrastructure infra;
	
	@EJB(beanName="PersistJPAMetricsBean")
	private PersistMetrics metrics;
	
	/**
	 * @return
	 */
	public static WebArchive createDeployment(String name) {
		return AbstractTest.createDeployment(name)
				.addClasses(
						// test class
						PersistJpaMetricsTest.class,
						// Services classes
						Infrastructure.class, InfrastructureBean.class, 
						PersistJPAMetricsBean.class, PersistMetrics.class)
				;
	}
	
	@Test
	public void testPersistJVM() throws CockpitException, IOException {
		List<JVM> jvms = infra.listDomainJVM(testDomain);
		
		metrics.saveBase(jvms);
		List<Metric> metrics = retreiveAll();
		
		check(jvms, metrics);
	}
	
	@Test
	public void testPersistOSB() throws CockpitException, IOException {
		List<OSBResource> jvms = infra.listDomainOSBResources(testDomain);
		
		metrics.saveBase(jvms);
		List<Metric> metrics = retreiveAll();
		
		check(jvms, metrics);
	}
	
	@Test
	public void testPersistOSBStatistics() throws CockpitException, IOException {
		List<OSBResource> jvms = infra.listDomainOSBStatistics(testDomain);
		
		metrics.saveBase(jvms);
		List<Metric> metrics = retreiveAll();
		
		check(jvms, metrics);
	}
	
	@Test
	public void testPersistApplication() throws CockpitException, IOException {
		List<Application> jvms = infra.listDomainApplications(null, testDomain);
		
		metrics.saveBase(jvms);
		List<Metric> metrics = retreiveAll();
		
		check(jvms, metrics);
	}
	
	/**
	 * Compare store result with original
	 * @param bases
	 * @param metrics
	 */
	private void check(List<? extends Base> bases, List<Metric> metrics) {
		
		// Get metrics from DB
		Calendar begin = Calendar.getInstance();
		begin.add(Calendar.MINUTE, -2);
		
		
		for(Base jvm: bases) {
			Metric ref = null;
			for(Metric metric: metrics)
				if (metric.getName().equals(jvm.getJmxObjectName()))
					ref = metric;
			
			assertNotNull("instance de Metric introuvable", ref);
			for(String key: jvm.getAttributeKeys()) {
				
				// Si la clée initiale est vide, on passe
				if ((jvm.getAttributeValue(key)== null) ||
					(jvm.getAttributeValue(key) instanceof Map))
					continue;
				
				// Taille trop importante ??
				if (
					(jvm.getAttributeValue(key).toString().length() < Metric.ValueMaxLength)
					) {
					
					// Les 2 attributs sont suprimés à la sauvegarde
					if (!(key.equals(Infrastructure.DomainName) 
							|| key.equals(Infrastructure.ServerName) 
							)
						) {
						assertNotNull("La valeur de "+key+" a disparu", ref.getValue(key));
					
						if ((jvm.getAttributeValue(key) instanceof Integer) || 
								(jvm.getAttributeValue(key) instanceof Long) || 
								(jvm.getAttributeValue(key) instanceof Float) || 
								(jvm.getAttributeValue(key) instanceof Double))
							assertEquals(Double.valueOf(jvm.getAttributeValue(key).toString()), ((NumericValue)ref.getValue(key)).getValue());
						else if (jvm.getAttributeValue(key) instanceof String)
							assertEquals("La valeur de "+key+" n'est pas correcte", jvm.getAttributeValue(key), ((TextValue)ref.getValue(key)).getValue());
						else
							assertEquals("La valeur de "+key+" n'est pas correcte", jvm.getAttributeValue(key).toString(), ((TextValue)ref.getValue(key)).getValue());
					}
				} else
					assertNull("La valeur de "+key+" doit être null", ref.getValue(key));
			}
		}
		
	}

	/**
	 * Find metrics on an interval
	 * @param begin
	 * @param end
	 * @return
	 */
	private List<Metric> retreive(Calendar begin, Calendar end) {
		return em.createNamedQuery(Metric.FindWithInterval)
			.setParameter("begin", begin)
			.setParameter("end", end)
			.getResultList();
	}
	
	private List<Metric> retreiveAll() {
		return em.createQuery("select m from Metric m")
			.getResultList();
	}
	
	@Test
	public void testStorePerformanceTrace() throws CockpitException {
		Calendar ref = Calendar.getInstance();
		
		// Entity to persist
		assertNotNull(em.getMetamodel().entity(Metric.class));
		assertNotNull(em.getMetamodel().entity(PerformanceTrace.class));
		
		// store value
		metrics.storePerformance("Test", ref, ref);
		
		// get value
		assertNotNull(em.find(PerformanceTrace.class, "Test"));
	}
	
	@Test
	public void testListAttributesByJmxType() throws CockpitException, IOException {
		// save data
		metrics.saveBase(infra.listDomainJVM(testDomain));
		
		Calendar start = Calendar.getInstance();
		start.add(Calendar.MINUTE, -20);

		List<NumericValue> nAllValues = metrics.listNumericValuesOfMetricByJmxType(ObjectNameFactory.JvmType, start, Calendar.getInstance());
		assertTrue("Numeric values are empty", nAllValues.size()>0);
		for(NumericValue nv: nAllValues)
			LOG.info(nv.getName()+"="+nv.getValue());
		
		List<NumericValue> nvalues = metrics.listNumericValuesOfMetricByJmxType("JVMRuntime", "HeapSizeMax", start, Calendar.getInstance());

		assertNotNull("Values are null", nvalues);
		assertEquals("HeapFreeCurrent value is not defined", 1, nvalues.size());
	}
	
	@Test
	public void testListAttributesByJmxName() throws CockpitException, IOException {
		// save data
		metrics.saveBase(infra.listDomainJVM(testDomain));
				
		Calendar start = Calendar.getInstance();
		start.add(Calendar.MINUTE, -10);
		
		List<NumericValue> nvalues = metrics.listNumericValuesOfMetricByJmxName("'com.bea:Location=AdminServer,Name=AdminServer,Type=ServerRuntime'", "HeapFreeCurrent", start, Calendar.getInstance());

		assertNotNull("Values are null", nvalues);
		assertTrue("Numeric values are empty", nvalues.size()>0);
	}
	
}
