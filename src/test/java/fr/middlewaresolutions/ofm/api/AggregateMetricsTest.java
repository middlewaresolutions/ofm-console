/**
 * 
 */
package fr.middlewaresolutions.ofm.api;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.ejb.BeforeCompletion;
import javax.ejb.EJB;
import javax.management.MalformedObjectNameException;

import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.middlewaresolutions.ofmconsole.api.AggregateMetrics;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.core.AggregateMetricsBean;
import fr.middlewaresolutions.ofmconsole.core.InfrastructureBean;
import fr.middlewaresolutions.ofmconsole.core.jpa.PersistJPAMetricsBean;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.MetricPer15Min;
import fr.middlewaresolutions.ofmconsole.metric.MetricPer5Min;
import fr.middlewaresolutions.ofmconsole.metric.MetricPerHour;
import fr.middlewaresolutions.ofmconsole.metric.NumericValuePer15Min;
import fr.middlewaresolutions.ofmconsole.metric.NumericValuePer5Min;
import fr.middlewaresolutions.ofmconsole.metric.NumericValuePerHour;

/**
 * @author Emmanuel
 *
 */
public class AggregateMetricsTest extends AbstractTest {

	@EJB
	private Infrastructure infra;
	
	@EJB(beanName="PersistJPAMetricsBean")
	private PersistMetrics metrics;
	
	@EJB
	private AggregateMetrics aggregate;
	
	/**
	 * @return
	 */
	public static WebArchive createDeployment(String name) {
		return AbstractTest.createDeployment(name)
				.addClasses(
						AggregateMetricsTest.class,
						Infrastructure.class, InfrastructureBean.class,
						PersistMetrics.class, PersistJPAMetricsBean.class,
						AggregateMetrics.class, AggregateMetricsBean.class
						)
				;
	}
	
	@Before
	public void loadData() throws IOException, CockpitException {
		metrics.saveBase(infra.listDomainServerChannelRuntimes(null, testDomain));
	}
	
	@Test
	public void testAggregatePer5Min() throws MalformedObjectNameException, CockpitException {
		
		// Aggregate data
		aggregate.aggregatePer5Min(Calendar.getInstance());
		
		List<MetricPer5Min> metrics = em.createQuery("select m from MetricPer5Min m").getResultList();
		
		for(MetricPer5Min metric: metrics) {
			assertNotNull(metric.getName());
			assertNotNull(metric.getJmx_name());
			assertNotNull(metric.getJmx_location());
			assertNotNull(metric.getJmx_serverRuntime());
			assertNotNull(metric.getJmx_type());
			
			for(NumericValuePer5Min numeric: metric.getNumericAttributes()) {
				assertNotNull(numeric.getName());
				assertNotNull(numeric.getAvg());
				assertNotNull(numeric.getMin());
				assertNotNull(numeric.getMax());
				assertNotNull(numeric.getEcartType());
				assertNotNull(numeric.getPercentil90());
				assertNotNull(numeric.getPercentile95());
				assertNotNull(numeric.getPercentile99());
				assertNotNull(numeric.getQuantity());
			}
		}
		
	}
	
	@Test
	public void testAggregatePer15Min() throws MalformedObjectNameException, CockpitException {
		
		// Aggregate data
		aggregate.aggregatePer15Min(Calendar.getInstance());
		
		List<MetricPer15Min> metrics = em.createQuery("select m from MetricPer15Min m").getResultList();
		
		for(MetricPer15Min metric: metrics)
			for(NumericValuePer15Min numeric: metric.getNumericAttributes()) {
				assertNotNull(numeric.getName());
				assertNotNull(numeric.getAvg());
				assertNotNull(numeric.getMin());
				assertNotNull(numeric.getMax());
				assertNotNull(numeric.getEcartType());
				assertNotNull(numeric.getPercentil90());
				assertNotNull(numeric.getPercentile95());
				assertNotNull(numeric.getPercentile99());
				assertNotNull(numeric.getQuantity());
			}
		
	}
	
	@Test
	public void testAggregatePerHour() throws MalformedObjectNameException, CockpitException {
//		// Persist WorkManager
//		metrics.saveBase(infra.listDomainWorkManager());
		
		// Aggregate data
		aggregate.aggregatePerHour(Calendar.getInstance());
		
		List<MetricPerHour> metrics = em.createQuery("select m from MetricPerHour m").getResultList();
		
		for(MetricPerHour metric: metrics)
			for(NumericValuePerHour numeric: metric.getNumericAttributes()) {
				assertNotNull(numeric.getName());
				assertNotNull(numeric.getAvg());
				assertNotNull(numeric.getMin());
				assertNotNull(numeric.getMax());
				assertNotNull(numeric.getEcartType());
				assertNotNull(numeric.getPercentil90());
				assertNotNull(numeric.getPercentile95());
				assertNotNull(numeric.getPercentile99());
				assertNotNull(numeric.getQuantity());
			}
		
	}
}
