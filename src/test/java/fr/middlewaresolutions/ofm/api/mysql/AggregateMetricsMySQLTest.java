/**
 * 
 */
package fr.middlewaresolutions.ofm.api.mysql;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofm.api.AggregateMetricsTest;

/**
 * @author Emmanuel
 *
 */
@RunWith(Arquillian.class)
public class AggregateMetricsMySQLTest extends AggregateMetricsTest {
	
	/**
	 * @return
	 */
	@Deployment
	public static WebArchive createDeployment() {
		return AggregateMetricsTest.createDeployment("AggregateMetricsTestMySQL")
				.addAsResource("persistence-mysql.xml", "META-INF/persistence.xml")
				;
	}
	
}
