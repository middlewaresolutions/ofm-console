/**
 * 
 */
package fr.middlewaresolutions.ofm.api.mysql;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofm.api.CleanJpaStorageTest;

/**
 * @author Emmanuel
 *
 */
@RunWith(Arquillian.class)
public class PersistJpaMetricsMySQLTest extends CleanJpaStorageTest {
	
	/**
	 * @return
	 */
	@Deployment(name="mysql")
	public static WebArchive createDeployment() {
		return CleanJpaStorageTest.createDeployment("PersistJpaMetricsTestMySQL")
			.addAsResource("persistence-mysql.xml", "META-INF/persistence.xml");
	}
	
}
