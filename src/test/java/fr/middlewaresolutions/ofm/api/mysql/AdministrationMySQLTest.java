/**
 * 
 */
package fr.middlewaresolutions.ofm.api.mysql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofm.api.AdministrationTest;
import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.administration.ejb.AdministrationBean;
import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.administration.model.Parameter;
import fr.middlewaresolutions.ofmconsole.core.ObjectNameFactory;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.model.Base;
import fr.middlewaresolutions.ofmconsole.performance.PerformanceTrace;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Emmanuel
 *
 */
@RunWith(Arquillian.class)
public class AdministrationMySQLTest extends AdministrationTest {
	
	/**
	 * @return
	 */
	@Deployment
	public static WebArchive createDeployment() {
		return AdministrationTest.createDeployment("AdministrationTestMySQL")
				.addAsResource("persistence-mysql.xml", "META-INF/persistence.xml")
				;
	}
	
}
