/**
 * 
 */
package fr.middlewaresolutions.ofm.api;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.persistence.metamodel.EntityType;

import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;

import fr.middlewaresolutions.ofmconsole.api.AggregateMetrics;
import fr.middlewaresolutions.ofmconsole.api.CleanStorage;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.core.AggregateMetricsBean;
import fr.middlewaresolutions.ofmconsole.core.InfrastructureBean;
import fr.middlewaresolutions.ofmconsole.core.ObjectNameFactory;
import fr.middlewaresolutions.ofmconsole.core.jpa.CleanJPAStorage;
import fr.middlewaresolutions.ofmconsole.core.jpa.PersistJPAMetricsBean;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.NumericValue;
import fr.middlewaresolutions.ofmconsole.metric.TextValue;
import fr.middlewaresolutions.ofmconsole.model.Application;
import fr.middlewaresolutions.ofmconsole.model.Base;
import fr.middlewaresolutions.ofmconsole.model.JVM;
import fr.middlewaresolutions.ofmconsole.model.OSBResource;
import fr.middlewaresolutions.ofmconsole.performance.PerformanceTrace;

/**
 * Tests for JPA Persistence
 * @author Emmanuel
 *
 */
public class CleanJpaStorageTest extends AbstractTest {

	@EJB
	private Infrastructure infra;
	
	@EJB
	private AggregateMetrics agg;
	
	@EJB
	private CleanStorage clean;
	
	@EJB(beanName="PersistJPAMetricsBean")
	private PersistMetrics metrics;
	
	/**
	 * @return
	 */
	public static WebArchive createDeployment(String name) {
		return AbstractTest.createDeployment(name)
				.addClasses(
						// test class
						CleanJpaStorageTest.class,
						// Services classes
						Infrastructure.class, InfrastructureBean.class, 
						PersistJPAMetricsBean.class, PersistMetrics.class,
						CleanStorage.class, CleanJPAStorage.class,
						AggregateMetrics.class, AggregateMetricsBean.class)
				;
	}
	
	@Test
	public void testCleanMetric() throws CockpitException, IOException {
		List<JVM> jvms = infra.listDomainJVM(testDomain);
		
		metrics.saveBase(jvms);
		assertTrue("Aucun élément à supprimer", (Long) em.createQuery("select count(m) from Metric m").getSingleResult() > 0);
		
		// Delete from Now - 1 hour
		Calendar start = Calendar.getInstance();
		start.add(Calendar.HOUR, -1);
		clean.cleanMetric(start, Calendar.getInstance());
		
		assertTrue("Il reste des Metric", (Long) em.createQuery("select count(m) from Metric m").getSingleResult() == 0);
	}
	
	@Test
	public void testCleanMetricPer5Min() throws CockpitException, IOException {
		Calendar past = Calendar.getInstance();
		past.add(Calendar.MINUTE, -10);
		
		// Change time to aggregate them
		List<JVM> jvms = infra.listDomainJVM(testDomain);
		for(JVM jvm: jvms)
			jvm.setTime(past);
		metrics.saveBase(jvms);
				
		agg.aggregatePer5Min(past);
		assertTrue("Aucun élément à supprimer", (Long) em.createQuery("select count(m) from MetricPer5Min m").getSingleResult() > 0);
		
		// Delete from Now - 1 hour
		Calendar start = Calendar.getInstance();
		start.add(Calendar.HOUR, -1);
		clean.cleanMetricPer5Min(start, Calendar.getInstance());
		
		assertTrue("Il reste des Metric", (Long) em.createQuery("select count(m) from MetricPer5Min m").getSingleResult() == 0);
	}
	
	@Test
	public void testCleanMetricPer15Min() throws CockpitException, IOException {
		Calendar past = Calendar.getInstance();
		past.add(Calendar.MINUTE, -20);
		
		// Change time to aggregate them
		List<JVM> jvms = infra.listDomainJVM(testDomain);
		for(JVM jvm: jvms)
			jvm.setTime(past);
		
		metrics.saveBase(jvms);
		agg.aggregatePer15Min(past);
		assertTrue("Aucun élément à supprimer", (Long) em.createQuery("select count(m) from MetricPer15Min m").getSingleResult() > 0);
		
		// Delete from Now - 1 hour
		Calendar start = Calendar.getInstance();
		start.add(Calendar.HOUR, -1);
		clean.cleanMetricPer15Min(start, Calendar.getInstance());
		
		assertTrue("Il reste des Metric", (Long) em.createQuery("select count(m) from MetricPer15Min m").getSingleResult() == 0);
	}
	
	@Test
	public void testCleanMetricPerHour() throws CockpitException, IOException {
		Calendar past = Calendar.getInstance();
		past.add(Calendar.MINUTE, -70);
		
		// Change time to aggregate them
		List<JVM> jvms = infra.listDomainJVM(testDomain);
		for(JVM jvm: jvms)
			jvm.setTime(past);
		
		metrics.saveBase(jvms);
		agg.aggregatePerHour(past);
		assertTrue("Aucun élément à supprimer", (Long) em.createQuery("select count(m) from MetricPerHour m").getSingleResult() > 0);
		
		// Delete from Now - 1 hour
		Calendar start = Calendar.getInstance();
		start.add(Calendar.HOUR, -2);
		clean.cleanMetricPerHour(start, Calendar.getInstance());
		
		assertTrue("Il reste des Metric", (Long) em.createQuery("select count(m) from MetricPerHour m").getSingleResult() == 0);
	}
}
