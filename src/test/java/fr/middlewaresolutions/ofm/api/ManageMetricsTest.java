/**
 * 
 */
package fr.middlewaresolutions.ofm.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import javax.ejb.EJB;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.administration.ejb.AdministrationBean;
import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.MetricsService;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.core.InfrastructureBean;
import fr.middlewaresolutions.ofmconsole.core.ObjectNameFactory;
import fr.middlewaresolutions.ofmconsole.core.jpa.JPABean;
import fr.middlewaresolutions.ofmconsole.core.jpa.PersistJPAMetricsBean;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.AbstractMetric;
import fr.middlewaresolutions.ofmconsole.metric.AbstractNumericValue;
import fr.middlewaresolutions.ofmconsole.metric.AbstractValue;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.NumericValue;
import fr.middlewaresolutions.ofmconsole.metric.TextValue;
import fr.middlewaresolutions.ofmconsole.model.Base;

/**
 * @author Emmanuel
 *
 */
public class ManageMetricsTest extends AbstractTest {

	@EJB
	private Infrastructure infra;
	
	@EJB(beanName="PersistJPAMetricsBean")
	private PersistMetrics metrics;
	
	/**
	 * @return
	 */
	public static WebArchive createDeployment(String name) {
		return AbstractTest.createDeployment(name)
				.addClasses(
						ManageMetricsTest.class,
						Infrastructure.class, InfrastructureBean.class, 
						PersistMetrics.class, PersistJPAMetricsBean.class)
				;
	}
	
	@Test
	public void testManageJVMMetrics() throws MalformedObjectNameException, CockpitException, IOException {
		
		// Insert data
		metrics.saveBase(infra.listDomainJVM(testDomain));
		
		// test
		ObjectName jvm = ObjectNameFactory.getJVMObjectName("DefaultServer");
		assertEquals("L'objectName n'est pas celui attendu", 
				"com.bea:Location=DefaultServer,Name=DefaultServer,ServerRuntime=DefaultServer,Type=JVMRuntime", jvm.getCanonicalName());
		
		for(Metric metric : metrics.lastMetricsOf(jvm)) {
			assertNotNull(metric.getValue("JavaVersion"));
			assertNotNull(metric.getValue("HeapSizeCurrent"));
			assertNotNull(metric.getValue("HeapFreeCurrent"));
			assertNotNull(metric.getValue("HeapSizeMax"));
		}
	}
	
	@Test
	public void testManageServerChannelsMetrics() throws MalformedObjectNameException, CockpitException, IOException {
		// Insert data
		metrics.saveBase(infra.listDomainServerChannelRuntimes(null, testDomain));
				
		String[] protocoles = {"http", "https", "iiop", "iiops", "ldap", "ldaps", "t3", "t3s"};
		
		for(String protocole: protocoles) {
			ObjectName http = ObjectNameFactory.getServerChannelObjectName("DefaultServer", "%["+protocole+"]%");

			assertNotNull( metrics.lastMetricsOf(http));
			
			for(Metric metric : metrics.lastMetricsOf(http)) {
				assertNotNull(metric.getValue("ConnectionsCount"));
				assertNotNull(metric.getValue("PublicURL"));
				assertNotNull(metric.getValue("AcceptCount"));
				assertNotNull(metric.getValue("MessagesReceivedCount"));
				assertNotNull(metric.getValue("MessagesSentCount"));
			}
		}
	}

	@Test
	public void testManageWorkManagerMetrics() throws MalformedObjectNameException, CockpitException, IOException {
		// Insert data
		metrics.saveBase(infra.listDomainWorkManager(testDomain));
		
		// test
		ObjectName on = ObjectNameFactory.getWorkManagerObjectName("DefaultServer", "%");
		
		for(Metric metric : metrics.lastMetricsOf(on)) {
			assertNotNull(metric.getValue("PendingRequests"));
			assertNotNull(metric.getValue("CompletedRequests"));
			assertNotNull(metric.getValue("StuckThreadCount"));
			
			assertNotNull(metric.getValue("MaxThreadsConstraintRuntime-ExecutingRequests"));
			assertNotNull(metric.getValue("MaxThreadsConstraintRuntime-PendingRequests"));
			assertNotNull(metric.getValue("MaxThreadsConstraintRuntime-CurrentWaitTime"));
			
			assertNotNull(metric.getValue("MinThreadsConstraintRuntime-ExecutingRequests"));
			assertNotNull(metric.getValue("MinThreadsConstraintRuntime-PendingRequests"));
			assertNotNull(metric.getValue("MinThreadsConstraintRuntime-CurrentWaitTime"));
		}
	}
	
}
