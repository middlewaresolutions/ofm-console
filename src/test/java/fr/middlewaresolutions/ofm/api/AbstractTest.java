package fr.middlewaresolutions.ofm.api;

import static org.junit.Assert.assertNotNull;

import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.AfterCompletion;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.administration.ejb.AdministrationBean;
import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.administration.model.Parameter;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.MetricsService;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.core.InfrastructureBean;
import fr.middlewaresolutions.ofmconsole.core.ObjectNameFactory;
import fr.middlewaresolutions.ofmconsole.core.jpa.JPABean;
import fr.middlewaresolutions.ofmconsole.core.jpa.PersistJPAMetricsBean;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.model.Base;
import fr.middlewaresolutions.ofmconsole.performance.PerformanceTrace;

public class AbstractTest {

	@PersistenceContext(unitName="administration")
	protected EntityManager emAdmin;
	
	@PersistenceContext(unitName="metrics")
	protected EntityManager em;
	
	@EJB
	protected Administration admin;
	
	@Resource
	UserTransaction ut;
	
	protected final Logger LOG = Logger.getLogger(this.getClass().getName());
	
	public AbstractTest() {
		// TODO Auto-generated constructor stub
	}

	protected Domain testDomain = null;
	
	public static WebArchive createDeployment(String name) {
		WebArchive archive = ShrinkWrap.create(WebArchive.class, name+".war")
				.addClasses(
						// test hierachy
						AbstractTest.class,
						// Admin class
						Administration.class, AdministrationBean.class, Domain.class,
						Parameter.class, PerformanceTrace.class,
						// Services classes
						ObjectNameFactory.class, CockpitException.class, MetricsService.class, JPABean.class
						)
				.addPackage(Base.class.getPackage())
				.addPackage(Metric.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("WEB-INF/weblogic.xml")
				.addAsLibraries(Maven.resolver()
						.resolve("org.eclipse.persistence:eclipselink:2.6.3", 
								"org.eclipse.persistence:org.eclipse.persistence.jpars:2.6.3",
								"org.apache.commons:commons-math3:3.6.1")
						.withTransitivity().asFile())
				;

		return archive;
	}
	
	@Before
	public void initTest() throws NotSupportedException, SystemException, SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
		if (admin.listDomainsToScan().size() == 0) {
			ut.begin();
			testDomain = new Domain("localhost", "7001", "weblogic", "welcome1");
			
			emAdmin.persist(testDomain);
			ut.commit();
		} else
			testDomain = admin.listDomainsToScan().get(0);
		
		assertNotNull(em);
		assertNotNull(emAdmin);
	}
	
	@After
	public void clearDB() {
		em.clear();
		emAdmin.clear();
	}
}
