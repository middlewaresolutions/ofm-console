/**
 * 
 */
package fr.middlewaresolutions.ofm.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.administration.ejb.AdministrationBean;
import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.administration.model.Parameter;
import fr.middlewaresolutions.ofmconsole.core.ObjectNameFactory;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.model.Base;
import fr.middlewaresolutions.ofmconsole.performance.PerformanceTrace;

/**
 * @author Emmanuel
 *
 */
public class AdministrationTest extends AbstractTest {
	
	/**
	 * @return
	 */
	public static WebArchive createDeployment(String name) {
		return AbstractTest.createDeployment(name)
				.addClasses(
						AdministrationTest.class
						)
				;

	}
	
	@Test
	public void testStoreConfigStringParameter() {
		
		// store value
		admin.storeConfigParameter("Test", "123");
		
		// get value
		assertEquals("123", admin.getConfigParameterValue("Test"));
	}
	
	@Test
	public void testStoreConfigDateParameter() {
		Calendar ref = Calendar.getInstance();
		
		// store value
		admin.storeConfigParameter("Calendar", ref);
		
		// get value
		assertTrue(ref.after(admin.getConfigParameterDate("Calendar")));
		assertTrue(!ref.before(admin.getConfigParameterDate("Calendar")));
	}
	
	@Test
	public void testListDomains() {
		assertEquals(1, admin.listDomainsToScan().size());
	}
}
