/**
 * 
 */
package fr.middlewaresolutions.ofm.api;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.administration.ejb.AdministrationBean;
import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.core.InfrastructureBean;
import fr.middlewaresolutions.ofmconsole.core.ObjectNameFactory;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.model.Application;
import fr.middlewaresolutions.ofmconsole.model.Base;
import fr.middlewaresolutions.ofmconsole.model.JMSDestination;
import fr.middlewaresolutions.ofmconsole.model.OSBResource;
import fr.middlewaresolutions.ofmconsole.model.SCAComposite;
import fr.middlewaresolutions.ofmconsole.model.Server;
import fr.middlewaresolutions.ofmconsole.model.ServerChannel;
import fr.middlewaresolutions.ofmconsole.model.WorkManager;

/**
 * @author Emmanuel
 *
 */
public class InfrastructureTest extends AbstractTest {

	@EJB
	private Infrastructure bean;
	
	/**
	 * @return
	 */
	public static WebArchive createDeployment(String name) {
		return AbstractTest.createDeployment(name)
				.addClasses(
						InfrastructureTest.class,
						Infrastructure.class, InfrastructureBean.class)
				;
	}
	
	@Test
	public void testListDomainServers() throws IOException {
		
		List<Server> servers = bean.listDomainServers(testDomain);
		assertNotNull(servers);
		assertTrue("Le nom n'est pas %Server", servers.get(0).getName().indexOf("Server")>0);
	}
	
	@Test
	public void testListApplications() throws IOException {
		List<Application> apps = bean.listDomainApplications(null, testDomain);
		assertNotNull(apps);
		for(Application app: apps) {
			assertNotNull(app.getName());
			assertNotNull(app.getHealth());
			
			assertNotNull(app.getAttributeValue(Infrastructure.ServerName));
			assertNotNull(app.getAttributeValue(Infrastructure.DomainName));
		}
		
		
	}
	
	@Test
	public void testServerChannelRuntimes() throws IOException {
		List<ServerChannel> apps = bean.listDomainServerChannelRuntimes(null, testDomain);
		assertNotNull(apps);
		for(ServerChannel app: apps) {
			assertNotNull(app.getName());
			assertNotNull(app.getHealth());
			
			assertNotNull(app.getAttributeValue(Infrastructure.ServerName));
			assertNotNull(app.getAttributeValue(Infrastructure.DomainName));
		}
		
		
	}

	@Test
	public void testWorkManagers() throws IOException {
		Collection<WorkManager> apps = bean.listDomainWorkManager(testDomain);
		assertNotNull(apps);
		assertTrue(apps.size() >0);
		
		for(WorkManager app: apps) {
			assertNotNull(app.getName());
			assertNotNull(app.getAttributeValue("PendingRequests"));
			assertNotNull(app.getAttributeValue("CompletedRequests"));
			
			assertNotNull(app.getAttributeValue(Infrastructure.ServerName));
			assertNotNull(app.getAttributeValue(Infrastructure.DomainName));
		}
		
		
	}
	
	@Test
	public void testJMS() throws IOException {
		Collection<JMSDestination> apps = bean.listDomainJMS(testDomain);
		assertNotNull(apps);
		assertTrue(apps.size() >0);
		
		for(JMSDestination app: apps) {
			assertNotNull(app.getName());
			assertNotNull(app.getAttributeValue("DestinationType"));
			assertNotNull(app.getAttributeValue("InsertionPaused"));
			
			assertNotNull(app.getAttributeValue(Infrastructure.ServerName));
			assertNotNull(app.getAttributeValue(Infrastructure.DomainName));
		}
		
		
	}
	
	@Test
	public void testResourcesOSB() throws IOException {
		List<OSBResource> resources = bean.listDomainOSBResources(testDomain);
		for(OSBResource osb: resources) {
			assertNotNull(osb.getName());
			
			assertNotNull(osb.getAttributeValue(Infrastructure.ServerName));
			assertNotNull(osb.getAttributeValue(Infrastructure.DomainName));
		}
	}
	
	@Test
	public void testResourcesOSBStatistics() throws IOException {
		List<OSBResource> resources = bean.listDomainOSBStatistics(testDomain);
		for(OSBResource osb: resources) {
			assertNotNull(osb.getName());
			assertNotNull(osb.getAttributeValue("Operation.process.message-count"));
			
			assertNotNull(osb.getAttributeValue(Infrastructure.ServerName));
			assertNotNull(osb.getAttributeValue(Infrastructure.DomainName));
		}
	}
	
	@Test
	public void testSCAComposites() throws IOException {
		Collection<SCAComposite> apps = bean.listDomainSCAComposite(testDomain);
		assertNotNull(apps);
		assertTrue("No SCA in weblogic.", apps.size() >0);
		
		for(SCAComposite app: apps) {
			assertNotNull(app.getName());
			
			assertNotNull(app.getAttributeValue(Infrastructure.ServerName));
			assertNotNull(app.getAttributeValue(Infrastructure.DomainName));
			
			assertNotNull(app.getServices());
			assertNotNull(app.getReferences());
		}
		
		
	}
}
