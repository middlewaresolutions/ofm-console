/**
 * 
 */
package fr.middlewaresolutions.ofm.api.derby;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofm.api.InfrastructureTest;

/**
 * @author Emmanuel
 *
 */
@RunWith(Arquillian.class)
public class InfrastructureDerbyTest extends InfrastructureTest {
	
	/**
	 * @return
	 */
	@Deployment(name="derby")
	public static WebArchive createDeployment() {
		return InfrastructureTest.createDeployment("InfrastructureTestDerby")
				.addAsResource("persistence-memory.xml", "META-INF/persistence.xml")
				;
	}

}
