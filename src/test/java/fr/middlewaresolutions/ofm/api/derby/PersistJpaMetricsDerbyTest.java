/**
 * 
 */
package fr.middlewaresolutions.ofm.api.derby;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofm.api.CleanJpaStorageTest;
import fr.middlewaresolutions.ofm.api.PersistJpaMetricsTest;

/**
 * @author Emmanuel
 *
 */
@RunWith(Arquillian.class)
public class PersistJpaMetricsDerbyTest extends PersistJpaMetricsTest {
	
	/**
	 * @return
	 */
	@Deployment(name="derby")
	public static WebArchive createDeployment() {
		return PersistJpaMetricsTest.createDeployment("PersistJpaMetricsTestDerby")
				.addAsResource("persistence-memory.xml", "META-INF/persistence.xml");
	}
	
}
