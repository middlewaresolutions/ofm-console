/**
 * 
 */
package fr.middlewaresolutions.ofm.api.derby;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofm.api.AggregateMetricsTest;

/**
 * @author Emmanuel
 *
 */
@RunWith(Arquillian.class)
public class AggregateMetricsDerbyTest extends AggregateMetricsTest {
	
	/**
	 * @return
	 */
	@Deployment
	public static WebArchive createDeployment() {
		return AggregateMetricsTest.createDeployment("AggregateMetricsTestDerby")
				.addAsResource("persistence-memory.xml", "META-INF/persistence.xml")
				;
	}
	
}
