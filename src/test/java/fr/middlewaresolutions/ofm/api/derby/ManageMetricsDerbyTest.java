/**
 * 
 */
package fr.middlewaresolutions.ofm.api.derby;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofm.api.ManageMetricsTest;

/**
 * @author Emmanuel
 *
 */
@RunWith(Arquillian.class)
public class ManageMetricsDerbyTest extends ManageMetricsTest {
	
	/**
	 * @return
	 */
	@Deployment
	public static WebArchive createDeployment() {
		return ManageMetricsTest.createDeployment("ManageMetricsTestDerby")
				.addAsResource("persistence-memory.xml", "META-INF/persistence.xml")
				;
	}
	
}
