/**
 * 
 */
package fr.middlewaresolutions.ofm.api.derby;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import fr.middlewaresolutions.ofm.api.CleanJpaStorageTest;

/**
 * @author Emmanuel
 *
 */
@RunWith(Arquillian.class)
public class CleanJpaStorageDerbyTest extends CleanJpaStorageTest {
	
	/**
	 * @return
	 */
	@Deployment(name="derby")
	public static WebArchive createDeployment() {
		return CleanJpaStorageTest.createDeployment("CleanJpaStorageDerbyTest")
				.addAsResource("persistence-memory.xml", "META-INF/persistence.xml");
	}
	
}
