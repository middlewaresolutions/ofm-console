SELECT m.time, count(*) FROM metrics-qual.metricperhour m group by m.time order by m.time desc; 

SELECT m.time, count(*) FROM metrics-qual.metricper15min m group by m.time order by m.time desc; 

SELECT m.time, count(*) FROM metrics-qual.metricper5min m group by m.time order by m.time desc;

SELECT m.time, m.JMX_TYPE, count(*) FROM metrics-qual.metric m group by m.time, m.JMX_TYPE order by m.time desc;

/* traces de performance */
SELECT * FROM performancetrace;

/* parametres */
SELECT * FROM `admin-qual`.parameter;