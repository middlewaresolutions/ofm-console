ALTER TABLE `metric` ROW_FORMAT = DYNAMIC ;
ALTER TABLE `numericvalue` ROW_FORMAT = DYNAMIC ;

ALTER TABLE `numericvalue` 
ADD INDEX `IDX_NumericValue1` (`NAME` ASC, `TIME` DESC);

ALTER TABLE `metric` 
ADD INDEX `IDX_Metric1` (`NAME` ASC, `TIME` DESC);

