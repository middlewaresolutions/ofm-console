select composite_name Composite, component_name Component, componenttype ComponentType,
DECODE(cube_instance.STATE,0, 'STATE_INITIATED',1, 'STATE_OPEN_RUNNING',2, 'STATE_OPEN_SUSPENDED',3, 'STATE_OPEN_FAULTED',4, 'STATE_CLOSED_PENDING_CANCEL',5, 'STATE_CLOSED_COMPLETED',6, 'STATE_CLOSED_FAULTED',7, 'STATE_CLOSED_CANCELLED',8, 'STATE_CLOSED_ABORTED',9, 'STATE_CLOSED_STALE',10,'STATE_CLOSED_ROLLED_BACK','unknown') state ,
extract(month from creation_date) M,
extract(day from creation_date) DD,
extract(hour from creation_date) HH,
extract(minute from creation_date) MM,
count(*) Instances,
count(*)/3600 Instances_ParSec,
      trunc(Max(extract(day    from (modify_date-creation_date))*24*60*60 +
                extract(hour   from (modify_date-creation_date))*60*60 +
                extract(minute from (modify_date-creation_date))*60 +
                extract(second from (modify_date-creation_date))),4) MaxTime,
      trunc(Min(extract(day    from (modify_date-creation_date))*24*60*60 +
                extract(hour   from (modify_date-creation_date))*60*60 + 
                extract(minute from (modify_date-creation_date))*60 +
                extract(second from (modify_date-creation_date))),4) MinTime,
      trunc(AVG(extract(day    from (modify_date-creation_date))*24*60*60 + 
                extract(hour   from (modify_date-creation_date))*60*60 + 
                extract(minute from (modify_date-creation_date))*60 +
                extract(second from (modify_date-creation_date))),4) AvgTime      
       from cube_instance
       where
        creation_date between (SYSTIMESTAMP-(10/24)) and SYSTIMESTAMP
 group by composite_name, component_name, componenttype, state, extract(month from creation_date), extract(day from creation_date), extract(hour from creation_date), extract(minute from creation_date)
order by extract(month from creation_date) asc, extract(day from creation_date) asc, extract(hour from creation_date) asc, extract(minute from creation_date) asc;