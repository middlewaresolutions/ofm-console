package fr.middlewaresolutions.ofmconsole.api;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.ejb.Local;
import javax.naming.NamingException;

import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.model.Application;
import fr.middlewaresolutions.ofmconsole.model.Cluster;
import fr.middlewaresolutions.ofmconsole.model.DMSMetric;
import fr.middlewaresolutions.ofmconsole.model.DataSource;
import fr.middlewaresolutions.ofmconsole.model.JMSDestination;
import fr.middlewaresolutions.ofmconsole.model.JVM;
import fr.middlewaresolutions.ofmconsole.model.OSBResource;
import fr.middlewaresolutions.ofmconsole.model.SCAComposite;
import fr.middlewaresolutions.ofmconsole.model.Server;
import fr.middlewaresolutions.ofmconsole.model.ServerChannel;
import fr.middlewaresolutions.ofmconsole.model.WorkManager;

/**
 * API Infrasucture
 * @author Emmanuel
 *
 */
@Local
public interface Infrastructure {
	
	String ServerName = "serverName";
	String DomainName = "domainName";

	List<Server> listDomainServers(Domain domain) throws IOException;

	List<Cluster> listDomainClusters(Domain domain) throws IOException;
	
	List<DataSource> listDomainDataSources(Domain domain) throws IOException;

	List<JMSDestination> listDomainJMS(Domain domain) throws IOException;

	List<Application> listDomainApplications(String TypeFilter, Domain domain) throws IOException;

	List<JVM> listDomainJVM(Domain domain) throws IOException;

	List<ServerChannel> listDomainServerChannelRuntimes(String filterChannels, Domain domain) throws IOException;

	List<SCAComposite> listDomainSCAComposite(Domain domain) throws IOException;

	Collection<WorkManager> listDomainWorkManager(Domain domain) throws IOException;

	List<DMSMetric> listDomainBpelEngine(Domain domain) throws IOException;

	List<DMSMetric> listDomainMessageProcessing(Domain domain) throws IOException;

	List<OSBResource> listDomainOSBResources(Domain domain) throws IOException;

	List<OSBResource> listDomainOSBResourcesWithStatistics(Domain domain) throws IOException;

	/**
	 * Get a vision of OSBStatistics
	 * @return
	 */
	List<OSBResource> listDomainOSBStatistics(Domain domain) throws IOException;

}
