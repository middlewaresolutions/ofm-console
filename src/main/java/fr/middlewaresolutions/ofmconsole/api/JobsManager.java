package fr.middlewaresolutions.ofmconsole.api;

import javax.ejb.Local;

@Local
public interface JobsManager {

	void stop();

	void start();

}
