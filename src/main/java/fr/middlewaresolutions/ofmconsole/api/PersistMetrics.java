package fr.middlewaresolutions.ofmconsole.api;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.ejb.Local;
import javax.management.ObjectName;

import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.MetricAttribute;
import fr.middlewaresolutions.ofmconsole.metric.NumericValue;
import fr.middlewaresolutions.ofmconsole.metric.Owner;
import fr.middlewaresolutions.ofmconsole.model.Base;

@Local
public interface PersistMetrics extends MetricsService {

	/**
	 * Save Base object
	 * @param jmxObjects
	 * @throws CockpitException 
	 */
	void saveBase(Collection<? extends Base> jmxObjects) throws CockpitException;

	/**
	 * USe JMX ObjectName to retreive metrics
	 * @param objectName
	 * @return
	 * @throws CockpitException 
	 */
	List<Metric> lastMetricsOf(ObjectName objectName) throws CockpitException;

	/**
	 * Use name to retreive Metric.
	 * Name with % could be used.
	 * @param partialObjectName
	 * @return
	 * @throws CockpitException 
	 */
	List<Metric> lastMetricsOfWithJmxName(String partialObjectName) throws CockpitException;

	/**
	 * Clear DB
	 * @throws CockpitException 
	 */
	void clear() throws CockpitException;

	/**
	 * Store performance information trace
	 * @param name
	 * @param start
	 * @param end
	 * @throws CockpitException 
	 */
	void storePerformance(String name, Calendar start, Calendar end) throws CockpitException;

	/**
	 * Get owner of metrics
	 * @param domain
	 * @param server
	 * @return
	 */
	Owner getOwner(String domain, String server);

	/**
	 * Store a metric description
	 * @param jmxFull
	 * @param jmxName
	 * @param jmxLocation
	 * @param jmxServerRuntime
	 * @param jmxType
	 * @param attributeName
	 */
	void storeMetricAttribute(String jmxFull, String jmxName, String jmxLocation, String jmxServerRuntime,
			String jmxType, String attributeName, boolean numeric);

	/**
	 * List all attribute for a metric
	 * @param jmxFullName
	 * @param isNumeric 
	 * @return
	 */
	List<MetricAttribute> listMetricAttributesByJmxFullName(String jmxFullName, boolean isNumeric);

	/**
	 * Get all attributes names for a JMX Type
	 * @param jmxType
	 * @return
	 */
	List<String> listAttributesNamesOfJmxType(String jmxType);

	/**
	 * Get last metrics of a specific JMX Type
	 * @param jmxType
	 * @return
	 * @throws CockpitException
	 */
	List<Metric> lastMetricsOfWithJmxType(String jmxType) throws CockpitException;

	/**
	 * List attributes of this metric
	 * @param jmxType
	 * @param attribute
	 * @return
	 */
	List<NumericValue> listNumericValuesOfMetricByJmxType(String jmxType, String attribute, Calendar begin, Calendar end);
	
	/**
	 * List attributes of this metric
	 * @param jmxType
	 * @param attribute
	 * @return
	 */
	List<NumericValue> listNumericValuesOfMetricByJmxName(String jmxName, String attribute, Calendar begin, Calendar end);

	/**
	 * List attributes metrics with a type JMX
	 * @param jvmType
	 * @return
	 */
	List<MetricAttribute> listMetricAttributesByJmxType(String jmxType, boolean isNumeric);

	<T> T find(Class<T> classe, Object primaryKey);

	/**
	 * Find all MetricAttributes of metric interval
	 * @param jmxType
	 * @param begin
	 * @param end
	 * @return
	 */
	List<NumericValue> listNumericValuesOfMetricByJmxType(String jmxType, Calendar begin, Calendar end);

}