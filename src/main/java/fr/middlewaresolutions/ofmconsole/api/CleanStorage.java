package fr.middlewaresolutions.ofmconsole.api;

import java.util.Calendar;

import javax.ejb.Local;

import fr.middlewaresolutions.ofmconsole.exception.CockpitException;

@Local
public interface CleanStorage {

	/**
	 * Clean entire storage from start to end.
	 * @param start
	 * @param end
	 * @throws CockpitException 
	 */
	void cleanMetric(Calendar start, Calendar end) throws CockpitException;
	
	/**
	 * Clean Metric5Min from start to end
	 * @param start
	 * @param end
	 */
	void cleanMetricPer5Min(Calendar start, Calendar end) throws CockpitException;
	
	/**
	 * Clean Metric15Min from start to end
	 * @param start
	 * @param end
	 */
	void cleanMetricPer15Min(Calendar start, Calendar end) throws CockpitException;
	
	/**
	 * Clean MetricPerHour from start to end
	 * @param start
	 * @param end
	 */
	void cleanMetricPerHour(Calendar start, Calendar end) throws CockpitException;
}
