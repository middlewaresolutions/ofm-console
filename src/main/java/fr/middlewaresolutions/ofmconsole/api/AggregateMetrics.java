package fr.middlewaresolutions.ofmconsole.api;

import java.util.Calendar;

import javax.ejb.Local;

import fr.middlewaresolutions.ofmconsole.exception.CockpitException;

@Local
public interface AggregateMetrics {

	Calendar aggregatePerMin(Calendar from);

	Calendar aggregatePer5Min(Calendar from) throws CockpitException;

	Calendar aggregatePer15Min(Calendar from) throws CockpitException;

	Calendar aggregatePerHour(Calendar from) throws CockpitException;

	Calendar aggregatePerDay(Calendar from) throws CockpitException;

}