/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.exception;

/**
 * Generic exception
 * @author Emmanuel
 *
 */
public class CockpitException extends Exception {

	/**
	 * 
	 */
	public CockpitException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public CockpitException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public CockpitException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CockpitException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CockpitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
