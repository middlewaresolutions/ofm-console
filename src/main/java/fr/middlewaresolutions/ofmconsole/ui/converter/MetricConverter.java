package fr.middlewaresolutions.ofmconsole.ui.converter;

import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.MetricAttribute;

@FacesConverter(value="MetricConverter", forClass=Metric.class)
public class MetricConverter implements Converter {
	
	@EJB
	PersistMetrics persist;
	
	public MetricConverter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null)
			return null;
		
		try {
			persist = InitialContext.doLookup("java:module/PersistJPAMetricsBean");
			
			return persist.find(Metric.class, Long.valueOf(value));
		} catch (NamingException e) {
			return null;
		}
		
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		return ((Metric)value).getId().toString();
	}

}
