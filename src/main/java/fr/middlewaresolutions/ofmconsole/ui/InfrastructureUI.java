/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.primefaces.event.SelectEvent;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.JobsManager;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.core.ObjectNameFactory;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.MetricAttribute;
import fr.middlewaresolutions.ofmconsole.model.Base;

/**
 * @author Emmanuel
 *
 */
@Named("infrastructureUI")
@javax.enterprise.context.SessionScoped
@Path("/api")
public class InfrastructureUI implements Serializable {

	private final Logger LOG = Logger.getLogger(this.getClass().getName());

	@EJB
	private Infrastructure infra;
	
	@EJB
	private Administration admin;
	
	@EJB(beanName="PersistJPAMetricsBean")
	private PersistMetrics store;
	
	@EJB
	private JobsManager manager;
	
	private Base current =null;
	
	private List<Metric> servers = null;
	
	/** Current domain for showing data */
	private Domain currentDomain;
	
	private Integer indexTabServer;
	
	@Inject @Named("area")
	private AreaChart chart;
	
	public InfrastructureUI() {
		
	}
	
	@GET
	@Path("/server")
	@Produces("application/json")
	public List<Metric> getServers() {
		if (servers == null){
			servers = new ArrayList<Metric>();

				try {
					servers.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.ServerType));
				} catch (CockpitException e) {
					// TODO Add JSF Message
					e.printStackTrace();
				}
			
		}
		
		return servers;
	}
	
	private List<Metric> clusters = null;
	
	@GET
	@Path("/cluster")
	@Produces("application/json")
	public List<Metric> getClusters() {
		if (clusters == null){
			clusters = new ArrayList<Metric>();

				try {
					clusters.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.ClusterType));
				} catch (CockpitException e) {
					// TODO Add JSF Message
					e.printStackTrace();
				}
			
		}
		
		return clusters;
	}
	
	private List<Metric> ds;
	
	@GET
	@Path("/datasource")
	@Produces("application/json")
	public List<Metric> getDataSources() {
		if (ds == null){
			ds = new ArrayList<Metric>();
		
			for(Metric server: getServers()) {
				try {
					ds.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.DatasourcesType));
				} catch (CockpitException e) {
					// TODO Add JSF Message
					e.printStackTrace();
				}
			}
		}
		
		return ds;
	}
	
	private List<Metric> jmsDestinations;
	
	@GET
	@Path("/jms")
	@Produces("application/json")
	public List<Metric> getJMSDestinations() throws CloneNotSupportedException {
		
		if (jmsDestinations == null){
			jmsDestinations = new ArrayList<Metric>();
		
				try {
					jmsDestinations.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.JmsDestinationType));
				} catch (CockpitException e) {
					// TODO Add JSF Message
					e.printStackTrace();
				}
			
		}
		
		return jmsDestinations;

	}
	
	private List<Metric> components;
	
	@GET
	@Path("/application/web")
	@Produces("application/json")
	public List<Metric> getApplicationWebComponents() {
		if (components == null){
			components = new ArrayList<Metric>();
		

				try {
					components.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.WebAppType));
				} catch (CockpitException e) {
					// TODO Add JSF Message
					e.printStackTrace();
				}
			
		}
		
		return components;
	}

	public Base getCurrent() {
		return current;
	}

	public void setCurrent(Base current) {
		this.current = current;
	}
	
	private List<Metric> jvms;
	
	@GET
	@Path("/server/jvm")
	@Produces("application/json")
	public List<Metric> getJVMs() {
		if (jvms== null) {
			jvms = new ArrayList<Metric>();
			
				try {
					jvms.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.JvmType));
				} catch (CockpitException e) {
					// TODO Add JSF Message
					e.printStackTrace();
				}
			
				
		}
		return jvms;
	}
	
	private List<Metric> http;
	
	@GET
	@Path("/server/http")
	@Produces("application/json")
	public List<Metric> getServerHttpChannels() throws MalformedObjectNameException {
		if (http == null) {
			String[] protocoles = {"http", "https"};
			http = new ArrayList<Metric>();
			
			for(Metric metric: getServers())
				for(String protocole: protocoles) {
					ObjectName protocol = ObjectNameFactory.getServerChannelObjectName(metric.getJmx_name(), "%["+protocole+"]%");
					try {
						http.addAll( store.lastMetricsOf(protocol));
					} catch (CockpitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		}
		
		return http;
	}
	
	private List<Metric> iiop;
	
	@GET
	@Path("/server/iiop")
	@Produces("application/json")
	public List<Metric> getServerIiopChannels() throws MalformedObjectNameException {
		if (iiop == null){
			String[] protocoles = {"iiop", "iiops"};
			iiop = new ArrayList<Metric>();
			
			for(Metric metric: getServers())
				for(String protocole: protocoles) {
					ObjectName protocol = ObjectNameFactory.getServerChannelObjectName(metric.getJmx_name(), "%["+protocole+"]%");
					try {
						iiop.addAll( store.lastMetricsOf(protocol));
					} catch (CockpitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		}
		
		return iiop;
	}
	
	private List<Metric> ldap;
	
	@GET
	@Path("/server/ldap")
	@Produces("application/json")
	public List<Metric> getServerLdapChannels() throws MalformedObjectNameException {
		if (ldap == null){
			String[] protocoles = {"ldap", "ldaps"};
			ldap = new ArrayList<Metric>();
			
			for(Metric metric: getServers())
				for(String protocole: protocoles) {
					ObjectName protocol = ObjectNameFactory.getServerChannelObjectName(metric.getJmx_name(), "%["+protocole+"]%");
					try {
						ldap.addAll( store.lastMetricsOf(protocol));
					} catch (CockpitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		}
		
		return ldap;
	}
	
	private List<Metric> t3;
	
	@GET
	@Path("/server/t3")
	@Produces("application/json")
	public List<Metric> getServerT3Channels() throws MalformedObjectNameException {
		if (t3 == null){
			String[] protocoles = {"t3", "t3s"};
			t3 = new ArrayList<Metric>();
			
			for(Metric metric: getServers())
				for(String protocole: protocoles) {
					ObjectName protocol = ObjectNameFactory.getServerChannelObjectName(metric.getJmx_name(), "%["+protocole+"]%");
					try {
						t3.addAll( store.lastMetricsOf(protocol));
					} catch (CockpitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		}
		
		return t3;
	}
	
	private List<Metric> snmp;
	
	@GET
	@Path("/server/snmp")
	@Produces("application/json")
	public List<Metric> getServerSnmpChannels() throws MalformedObjectNameException {
		if (snmp == null){
			String[] protocoles = {"snmp", "snmps"};
			snmp = new ArrayList<Metric>();
			
			for(Metric metric: getServers())
				for(String protocole: protocoles) {
					ObjectName protocol = ObjectNameFactory.getServerChannelObjectName(metric.getJmx_name(), "%["+protocole+"]%");
					try {
						snmp.addAll( store.lastMetricsOf(protocol));
					} catch (CockpitException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		}
		
		return snmp;
	}
	
	/**
	 * Convert bytes to MegaBytes
	 * @param octets
	 * @return
	 */
	public Integer toMB(Integer octets) {
		return Math.abs(octets/(1024*1024));
	}
	
	private List<Metric> composites;
	@GET
	@Path("/soa/composite")
	@Produces("application/json")
	public List<Metric> getSCAComposites() {
		if (composites == null) {
			composites = new ArrayList<Metric>();

			try {
				composites.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.ScaType));
			} catch (CockpitException e) {
				// TODO Add JSF Message
				e.printStackTrace();
			}

		}
		
		return composites;
	}
	
	private List<Metric> services;
	@GET
	@Path("/soa/composite/services")
	@Produces("application/json")
	public List<Metric> getSCAServices() {
		if (services == null) {
			services = new ArrayList<Metric>();

			try {
				services.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.ScaServicesType));
			} catch (CockpitException e) {
				// TODO Add JSF Message
				e.printStackTrace();
			}

		}
		return services;
	}
	
	private List<Metric> references;
	@GET
	@Path("/soa/composite/references")
	@Produces("application/json")
	public List<Metric> getSCAReferences() {
		if (references == null){
			references = new ArrayList<Metric>();

			try {
				references.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.ScaReferenceType));
			} catch (CockpitException e) {
				// TODO Add JSF Message
				e.printStackTrace();
			}

		}
		
		return references;
	}
	
	private List<Metric> wm;
	
	@GET
	@Path("/server/workmanager")
	@Produces("application/json")
	public List<Metric> getWorkManagers() throws MalformedObjectNameException {
		if (wm == null) {
			wm = new ArrayList<Metric>();
			
			for(Metric metric: getServers()) {
				ObjectName onWM = ObjectNameFactory.getWorkManagerObjectName(metric.getJmx_name(), "%");
				try {
					wm.addAll( store.lastMetricsOf(onWM));
				} catch (CockpitException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return wm;
	}
	
	private List<Metric> bpelEngine;
	
	@GET
	@Path("/soa/engine")
	@Produces("application/json")
	public List<Metric> getBpelEngine() {
		if (bpelEngine == null){
			bpelEngine = new ArrayList<Metric>();
		
				try {
					bpelEngine.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.BpelRequestType));
				} catch (CockpitException e) {
					// TODO Add JSF Message
					e.printStackTrace();
				}
			
		}
		return bpelEngine;
	}
	
	private List<Metric> messageProcessing;
	
	@GET
	@Path("/soa/messageprocessing")
	@Produces("application/json")
	public List<Metric> getMessageProcessing() {
		if (messageProcessing == null){
			messageProcessing = new ArrayList<Metric>();
		
				try {
					messageProcessing.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.MessageProcessingType));
				} catch (CockpitException e) {
					// TODO Add JSF Message
					e.printStackTrace();
				}
			
		}
		
		return messageProcessing;
	}
	
	private List<Metric> osbResources;
	private List<Metric> osbProxy;
	private List<Metric> osbBusiness;
	private List<Metric> osbPipeline;
	private List<Metric> osbFlow;
	
	@GET
	@Path("/osb/resources")
	@Produces("application/json")
	public List<Metric> getOsbResources() {
		
		if (osbResources == null) {
			try {
				osbResources = new ArrayList<Metric>();
				
				osbResources.addAll(store.lastMetricsOfWithJmxType(ObjectNameFactory.OsbType));
				
				osbProxy = new ArrayList<Metric>();
				osbBusiness = new ArrayList<Metric>();
				osbPipeline = new ArrayList<Metric>();
				osbFlow = new ArrayList<Metric>();
				
				for(Metric resource: osbResources) {
					switch((String)resource.getAttributeValue("Type")) {
						case "FLOW": 
							osbFlow.add(resource);
							break;
						case "Pipeline": 
							osbPipeline.add(resource);
							break;
						case "ProxyService": 
							osbProxy.add(resource);
							break;
						case "BusinessService": 
							osbBusiness.add(resource);
							break;
						default:
							LOG.warning("OSBResource Type unknown: "+(String)resource.getAttributeValue("Type"));
					}
				}
			} catch (CockpitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return osbResources;
	}
	
	@GET
	@Path("/osb/proxy")
	@Produces("application/json")
	public List<Metric> getOsbProxy() {
		if (osbProxy == null) getOsbResources();
		return osbProxy;
	}
	
	@GET
	@Path("/osb/business")
	@Produces("application/json")
	public List<Metric> getOsbBusiness() {
		if (osbBusiness == null) getOsbResources();
		return osbBusiness;
	}
	
	@GET
	@Path("/osb/flow")
	@Produces("application/json")
	public List<Metric> getOsbFlow() {
		if (osbFlow == null) getOsbResources();
		return osbFlow;
	}
	
	@GET
	@Path("/osb/pipeline")
	@Produces("application/json")
	public List<Metric> getOsbPipeline() {
		if (osbPipeline == null) getOsbResources();
		return osbPipeline;
	}
	
	/**
	 * Clear DB
	 */
	public void clearStorage() {
		try {
			store.clear();
		} catch (CockpitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void stopJobs() {
		manager.stop();
	}
	
	public void startJobs() {
		manager.start();
	}
	
	public void traceEvent(SelectEvent event) {
		LOG.fine("Event");
		
		FacesMessage msg = new FacesMessage("Selected ", "Details");
        FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public Integer getIndexTabServer() {
		return indexTabServer;
	}

	public void setIndexTabServer(Integer indexTab) {
		this.indexTabServer = indexTab;
	}
}
