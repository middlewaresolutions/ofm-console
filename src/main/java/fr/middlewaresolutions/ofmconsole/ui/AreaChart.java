/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.ui;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.inject.Named;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.MetricAttribute;
import fr.middlewaresolutions.ofmconsole.metric.NumericValue;
import fr.middlewaresolutions.ofmconsole.metric.Owner;

/**
 * @author Emmanuel
 *
 */
@Named("area")
@javax.enterprise.context.SessionScoped
public class AreaChart implements Serializable {

	@EJB(beanName="PersistJPAMetricsBean")
	private PersistMetrics store;
	
	@EJB
	Administration admin;
	
	private LineChartModel model;
	
	private List<MetricAttribute> metricAttributes = new ArrayList<>();
	private List<String> metricId = new ArrayList<>();
	
	private List<Metric> selectedJmxMetrics = new ArrayList<>();
	
	private Logger LOG = Logger.getLogger(this.getClass().getName());
	
	/**
	 * 
	 */
	public AreaChart() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Model used by UI Component
	 * @param minutesBack
	 * @return
	 */
	public LineChartModel getModel(int minutesBack) {

		if (model != null)
			return model;
		
		// Chart on last 30min
		Calendar end = Calendar.getInstance();
		Calendar start = Calendar.getInstance();
		start.add(Calendar.MINUTE, -minutesBack);
		
		// build graph
		build(start, end, attributes);
		
		return model;
	}

	ArrayList<String[]> attributes = new ArrayList<>();
	public void addAttribute(String type, String name) {
		attributes.add(new String[] {type, name});
	}
	
	public void setModel(LineChartModel model) {
		this.model = model;
	}

	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	/**
	 * Build graph
	 * 
	 * @param jmxType
	 * @param start
	 * @param end
	 * @param attributes
	 */
	private void build(Calendar start, Calendar end, ArrayList<String[]> attributes) {
		
		model= new LineChartModel();
		LOG.info("Start="+sdf.format(start.getTime()));
		LOG.info("End="+sdf.format(end.getTime()));
		
		boolean nodata = true;
		
		for (String[] attribute: attributes) {
			LineChartSeries series1 = new LineChartSeries();
			series1.setLabel(attribute[1]);
			model.addSeries(series1);
			
			LOG.info("Show Values for "+attribute[0]+":"+attribute[1]+" at "+sdf.format(end.getTime())+" interval of "+(end.getTimeInMillis()-start.getTimeInMillis())+" ms");
			
			for(NumericValue value: store.listNumericValuesOfMetricByJmxType(attribute[0], attribute[1], start, end)) {
				series1.set(sdf.format(value.getTime().getTime()), value.getValue());
				nodata = false;
			}
		}
		
		if (nodata) {
			LineChartSeries series1 = new LineChartSeries();
			series1.setLabel("No data");
			model.addSeries(series1);
			series1.set(sdf.format(Calendar.getInstance().getTime()), 0);
		}
		
		model.setTitle("Zoom for Details");
		model.getAxis(AxisType.Y).setLabel("Values");
		model.setLegendCols(1);
		model.setLegendPosition("e");
		model.setLegendRows(model.getSeries().size());
		
		DateAxis axis = new DateAxis("Dates");
		axis.setTickAngle(-50);
		axis.setMax(sdf.format(Calendar.getInstance().getTime()));
	    axis.setTickFormat("%H:%#M:%S");
		model.getAxes().put(AxisType.X, axis);
	}

	public List<MetricAttribute> getMetricAttributes() {
		return metricAttributes;
	}

	public void setMetricAttributes(List<MetricAttribute> metricAttributes) {
		this.metricAttributes.addAll(metricAttributes);
		
		for(MetricAttribute metricAtt: metricAttributes)
			addAttribute(metricAtt.getJmxType(), metricAtt.getAttributeName());
	}

	public List<String> getMetricAttributeId() {
		return metricId;
	}

	public void setMetricAttributeId(List<String> metricId) {
		clear();
		
		this.metricId = metricId;
		
		// add all metrics attribute
		for(String id: metricId) {
			MetricAttribute ma = store.find(MetricAttribute.class, Long.valueOf(id));
			metricAttributes.add(ma);
			
			// add attribute in string vision
			addAttribute(ma.getJmxType(), ma.getAttributeName());
		}
	}

	public List<Metric> getSelectedJmxMetrics() {
		return selectedJmxMetrics;
	}

	public void setSelectedJmxMetrics(List<Metric> selectedJmxMetrics) {
		this.selectedJmxMetrics = selectedJmxMetrics;
	}
	
	public Metric getMetric() {
		return (selectedJmxMetrics.size() ==1)? selectedJmxMetrics.get(0): null;
	}

	public void setMetric(Metric metric) {
		this.selectedJmxMetrics.clear();
		this.selectedJmxMetrics.add(metric);
		
		// RAZ model
		model = null;
	}
	
	/**
	 * List all attributes metrics of all metrics selected in area graph
	 * @return
	 */
	public List<MetricAttribute> listMetricAttributesOfMetricsGraph() {
		List<MetricAttribute> ma = new ArrayList<>();
		
		for(Metric metric: getSelectedJmxMetrics())
			ma.addAll(store.listMetricAttributesByJmxFullName(metric.getName(), true));
			
		return ma;
	}
	
	/**
	 * Clean model with attributes
	 */
	public void clear() {
		if (model != null)
			model.clear();
		model = null;
		
		attributes.clear();
		metricAttributes.clear();
		metricId.clear();
	}
	
	/**
	 * RAZ all datas
	 */
	public void raz() {
		clear();
		selectedJmxMetrics.clear();
	}
}
