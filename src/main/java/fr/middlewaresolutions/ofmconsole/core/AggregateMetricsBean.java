/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.core;

import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.eclipse.persistence.queries.ScrollableCursor;

import fr.middlewaresolutions.ofmconsole.api.AggregateMetrics;
import fr.middlewaresolutions.ofmconsole.core.jpa.JPABean;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.AbstractMetric;
import fr.middlewaresolutions.ofmconsole.metric.AbstractStatisticValue;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.MetricPer15Min;
import fr.middlewaresolutions.ofmconsole.metric.MetricPer5Min;
import fr.middlewaresolutions.ofmconsole.metric.MetricPerDay;
import fr.middlewaresolutions.ofmconsole.metric.MetricPerHour;
import fr.middlewaresolutions.ofmconsole.metric.NumericValue;
import fr.middlewaresolutions.ofmconsole.metric.NumericValuePer15Min;
import fr.middlewaresolutions.ofmconsole.metric.NumericValuePer5Min;
import fr.middlewaresolutions.ofmconsole.metric.NumericValuePerDay;
import fr.middlewaresolutions.ofmconsole.metric.NumericValuePerHour;

/**
 * Aggergate all metrics in function of collect
 * @author Emmanuel
 *
 */
@Stateless(name="AggregateMetricsBean", mappedName="AggregateMetricsBean")
@TransactionManagement(TransactionManagementType.BEAN)
public class AggregateMetricsBean extends JPABean implements AggregateMetrics {
	
	@Resource
	UserTransaction ut;
	
	private Logger LOG = Logger.getLogger(this.getClass().getName());
	
	/** Intervals managed */
	private final static int FIVE = 5;
	private final static int FIFTEEN = 15;
	private final static int HOUR = 60;
	private final static int DAY = 1440;
	
	/**
	 * Get Calendar with a delta in minute.
	 * If is past, delta should be negative.
	 * 
	 * @param minuteDelta
	 * @return
	 */
	private Calendar getLastCalendarPer5Min(Calendar refCalendar) {
		Calendar start = Calendar.getInstance();
		start.setTime(refCalendar.getTime());
		int min = start.get(Calendar.MINUTE);
		
		if (min == 0) {
			min = 55;
			// Past
			start.add(Calendar.HOUR, -1);
		} else if (min < 5)
			min = 0;
		else if (min < 10)
			min = 5;
		else if (min < 15)
			min = 10;
		else if (min < 20)
			min = 15;
		else if (min < 25)
			min = 20;
		else if (min < 30)
			min = 25;
		else if (min < 35)
			min = 30;
		else if (min < 40)
			min = 35;
		else if (min < 45)
			min = 40;
		else if (min < 50)
			min = 45;
		else if (min < 55)
			min = 50;
		else if (min < 60)
			min = 55;
		
		start.set(Calendar.MINUTE, min);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);
		
		return start;
	}
	
	/**
	 * End of interval for a quarter
	 * @param refCalendar
	 * @return
	 */
	private Calendar getLastCalendarPer15Min(Calendar refCalendar) {
		Calendar start = Calendar.getInstance();
		start.setTime(refCalendar.getTime());
		int min = start.get(Calendar.MINUTE);
		
		if (min == 0) {
			min = 45;
			// Past
			start.add(Calendar.HOUR, -1);
		} else if ( min < 15 )
			min = 0;
		else  if ( min < 30 )
			min = 15;
		else if ( min <= 45 )
			min = 30;
		else 
			min = 45;
		
		start.set(Calendar.MINUTE, min);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);
		
		return start;
	}
	
	/**
	 * End of interval for an hour
	 * @param refCalendar
	 * @return
	 */
	private Calendar getLastCalendarPerHour(Calendar refCalendar) {
		Calendar start = Calendar.getInstance();
		start.setTime(refCalendar.getTime());
		
		// Start of hour
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);
		
		// Last hour 
		start.add(Calendar.HOUR, -1);
		
		return start;
	}
	
	/**
	 * End of interval for a day
	 * @param refCalendar
	 * @return
	 */
	private Calendar getLastCalendarPerDay(Calendar refCalendar) {
		Calendar start = Calendar.getInstance();
		start.setTime(refCalendar.getTime());
		
		// Start of hour
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 0);
		start.set(Calendar.MILLISECOND, 0);
		
		// Last hour 
		start.add(Calendar.HOUR, -1);
		
		return start;
	}
	
	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.core.AggregateMetrics#aggregatePerMin()
	 */
	@Override
	public Calendar aggregatePerMin(Calendar from) {
		LOG.fine("Nothing to aggregate");
		
		return from;
	}
	
	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.core.AggregateMetrics#aggregatePer5Min()
	 */
	@Override
	public Calendar aggregatePer5Min(Calendar from) throws CockpitException {
		Calendar now = Calendar.getInstance();
		
		int max = 4;
		int nb = 0;
		
		// Repeat for each interval
		do {
			LOG.info("Start Aggregate 5Min from "+from.getTime());
			
			aggregatePerIntervalMinAtTime(from, FIVE);
			
			// Add 5 minutes
			from.add(Calendar.MINUTE, FIVE);
			
			// Repeat until from is futur
		} while (from.before(now)  && (nb++ <=max));
		
		return from;
	}
	
	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.core.AggregateMetrics#aggregatePer15Min()
	 */
	@Override
	public Calendar aggregatePer15Min(Calendar from) throws CockpitException {
		Calendar now = Calendar.getInstance();
		
		int max = 4;
		int nb = 0;
		// Repeat for each interval
		do {
			LOG.info("Start Aggregate 15Min from "+from.getTime());
			
			aggregatePerIntervalMinAtTime(from, FIFTEEN);
			
			// Add 15 minutes
			from.add(Calendar.MINUTE, FIFTEEN);
			
			// Repeat until from is futur
		} while (from.before(now) && (nb++ <=max));
		
		return from;
	}
	
	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.core.AggregateMetrics#aggregatePerHour()
	 */
	@Override
	public Calendar aggregatePerHour(Calendar from) throws CockpitException {
		Calendar now = Calendar.getInstance();
		
		int max = 4;
		int nb = 0;
		
		// Repeat for each interval
		do {
			LOG.info("Start Aggregate Hour from "+from.getTime());
			
			aggregatePerIntervalMinAtTime(from, HOUR);
			
			// Add 60 minutes
			from.add(Calendar.MINUTE, HOUR);
			
			// Repeat until from is futur
		} while (from.before(now) && (nb++ <=max));
		
		return from;
	}

	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.core.AggregateMetrics#aggregatePerDay()
	 */
	@Override
	public Calendar aggregatePerDay(Calendar from) throws CockpitException {
		Calendar now = Calendar.getInstance();
		
		int max = 4;
		int nb = 0;
		
		// Repeat for each interval
		do {
			LOG.info("Start Aggregate Day from "+from.getTime());
			
			aggregatePerIntervalMinAtTime(from, DAY);
			
			// Add 1440 minutes
			from.add(Calendar.MINUTE, DAY);
			
			// Repeat until from is futur
		} while (from.before(now)  && (nb++ <=max));
		
		return from;
	}
	
	/**
	 * Aggregate for 5 min values
	 * @param refDate
	 * @throws CockpitException 
	 */
	private void aggregatePerIntervalMinAtTime(Calendar refDate, int intervalInMinute) throws CockpitException {
		Calendar end = null;
		
		switch (intervalInMinute) {
			case FIVE: 
				end = getLastCalendarPer5Min(refDate);
				break;
			case FIFTEEN:
				end = getLastCalendarPer15Min(refDate);
				break;
			case HOUR:
				end = getLastCalendarPerHour(refDate);
				break;
			case DAY:
				end = getLastCalendarPerDay(refDate);
				break;
			default:
		}
			
		
		
		// Start is End - intervalInMinute min
		Calendar start = Calendar.getInstance();
		start.setTime(end.getTime());
		start.add(Calendar.MINUTE, -intervalInMinute);
		
		Query nqMetrics = em.createQuery("select distinct m.name, m.jmx_name, m.jmx_location, m.jmx_serverRuntime, m.jmx_type from Metric m where m.time between :begin and :end");
		
		// Retreive all metrics to aggregate
		List<Object[]> metricNames = nqMetrics
				.setParameter("begin", start)
				.setParameter("end", end)
				.getResultList();
		
		// Get statistics for each metric name
		for(Object[] metricName: metricNames) {
			
			// Data set
			DescriptiveStatistics summary = new DescriptiveStatistics(DescriptiveStatistics.INFINITE_WINDOW);
					
			AbstractMetric newMetric = null;
			switch (intervalInMinute) {
				case FIVE: 
					newMetric = new MetricPer5Min();
					break;
				case FIFTEEN:
					newMetric = new MetricPer15Min();
					break;
				case HOUR:
					newMetric = new MetricPerHour();
					break;
				case DAY:
					newMetric = new MetricPerDay();
					break;
				default:
			}
			
			newMetric.setName((String)metricName[0]);
			newMetric.setJmx_name((String)metricName[1]);
			newMetric.setJmx_location((String)metricName[2]);
			newMetric.setJmx_serverRuntime((String)metricName[3]);
			newMetric.setJmx_type((String)metricName[4]);

			// set Day
			switch (intervalInMinute) {
				case FIVE:
					((MetricPer5Min)newMetric).setDay(end);
					break;
				case FIFTEEN:
					((MetricPer15Min)newMetric).setDay(end);
					break;
				case HOUR:
					((MetricPerHour)newMetric).setDay(end);
					break;
				case DAY:
					((MetricPerDay)newMetric).setDay(end);
					break;
				default:
			}
			
			LOG.fine("Statistic metric "+intervalInMinute+"min analysis for "+newMetric.getName());
			
			Query nqNumericAttributesName = em.createNamedQuery(Metric.GetNumericAttributeNamesInInterval);
			
			// Retreive all attribute for the metric
			List<String> attrNumericNames = nqNumericAttributesName
					.setParameter("name", newMetric.getName())
					.setParameter("begin", start)
					.setParameter("end", end)
					.getResultList();
			
			// Add a statistic vision of all values
			for(String attrName: attrNumericNames) {
				AbstractStatisticValue value = null;
				
				switch (intervalInMinute) {
					case FIVE:
						value = new NumericValuePer5Min();
						((MetricPer5Min)newMetric).addNumericAttribute(attrName, (NumericValuePer5Min) value);
						break;
					case FIFTEEN:
						value = new NumericValuePer15Min();
						((MetricPer15Min)newMetric).addNumericAttribute(attrName, (NumericValuePer15Min) value);
						break;
					case HOUR:
						value = new NumericValuePerHour();
						((MetricPerHour)newMetric).addNumericAttribute(attrName, (NumericValuePerHour) value);
						break;
					case DAY:
						value = new NumericValuePerDay();
						((MetricPerDay)newMetric).addNumericAttribute(attrName, (NumericValuePerDay) value);
						break;
					default:
						throw new CockpitException("Bad interval of "+intervalInMinute);
				}
					
				value.setName(attrName);
				
				
				// RAZ summary
				summary.clear();
				
				Query nqNumericAttributes = em.createNamedQuery(Metric.GetNumericAttributesByNameInInterval);
				
				// Avoid MemoryLeak. Doing Pagination
				nqNumericAttributes.setHint("eclipselink.cursor.scrollable", true);
				
				// Retreive attributes
				nqNumericAttributes
						.setParameter("metricName", newMetric.getName())
						.setParameter("attributeName", attrName)
						.setParameter("begin", start)
						.setParameter("end", end);
				
				ScrollableCursor scrollableCursor = (ScrollableCursor)nqNumericAttributes.getSingleResult();
				
				// Use a scrollable cursor, to limit heap usage
				while (scrollableCursor.hasNext()) {
					NumericValue numeric = (NumericValue) scrollableCursor.next();
				
					summary.addValue(numeric.getValue());
				};
				scrollableCursor.close();
				
				// Collect statistics
				value.setQuantity(summary.getN());
				value.setAvg(summary.getSum()/summary.getN());
				value.setMin(summary.getMin());
				value.setMax(summary.getMax());
				value.setEcartType(summary.getStandardDeviation());
				value.setPercentil90(summary.getPercentile(90));
				value.setPercentile95(summary.getPercentile(95));
				value.setPercentile99(summary.getPercentile(99));
			}
			
			try {	
				// One transaction per metric
				ut.begin();
				
				// store metric
				em.merge(newMetric);
				
				ut.commit();
			} catch (MathIllegalArgumentException | SecurityException | IllegalStateException | NotSupportedException
					| SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException e) {
				LOG.warning(e.getMessage());
				try {
					ut.rollback();
				} catch (IllegalStateException | SecurityException | SystemException e1) {
					LOG.warning(e.getMessage());
				}
			}
		}
	}
	
	

}
