/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.core;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

/**
 * Factory of JMX ObjectName
 * @author Emmanuel
 *
 */
public abstract class ObjectNameFactory {

	public static final String ClusterType = "ClusterRuntime";

	public static final String OsbStatisticType = "OSBStatistic";

	public static final String ScaReferenceType = null;

	public static final String ScaServicesType = null;
	
	public static String DomainType = "Domain";
	
	/**
	 * Get new ObjectName for a Domain
	 * @param domainName
	 * @return
	 * @throws MalformedObjectNameException
	 */
	public static ObjectName getDomainObjectName(String domainName) throws MalformedObjectNameException {
		return new ObjectName("com.bea:Name=" + domainName + ",Type=Domain");
	}
	
	public static String ServerType = "ServerRuntime";
	
	/**
	 * Get new ObjectName for a Server
	 * @param serverName
	 * @return
	 * @throws MalformedObjectNameException
	 */
	public static ObjectName getServerObjectName(String serverName) throws MalformedObjectNameException {
		return new ObjectName("com.bea:Name="+serverName+",Location="+serverName+",Type=ServerRuntime");
	}
	
	public static String DatasourcesType = "JDBCDataSourceRuntime";
	
	/**
	 * Get new ObjectName for a DataSource
	 * @param dsName
	 * @return
	 * @throws MalformedObjectNameException
	 */
	public static ObjectName getDataSourceObjectName(String serverName, String dsName) throws MalformedObjectNameException {
		return new ObjectName(
				"com.bea:ServerRuntime="+serverName+",Name="+dsName+",Location="+serverName+",Type=JDBCDataSourceRuntime"
				);
	}
	
	public static String JmsDestinationType = "JMSDestinationRuntime";
	public static String JmsServerType = "JMSServerRuntime";
	
	/**
	 * Get new ObjectName for a JMSServer
	 * @param serverName
	 * @param jmsServerName
	 * @return
	 * @throws MalformedObjectNameException
	 */
	public static ObjectName getJMSServerObjectName(String serverName, String jmsServerName) throws MalformedObjectNameException {
		return new ObjectName(
				"com.bea:ServerRuntime="+serverName+",Name="+jmsServerName+",Location="+serverName+",Type=JMSServerRuntime"
				);
	}
	
	public static String ApplicationType = "ApplicationRuntime";
	public static String WebAppType = "WebAppComponentRuntime";
	public static String EjbAppType = "EJBComponentRuntime";
	public static String JmsAppType = "JMSComponentRuntime";
	
	/**
	 * Get new ObjectName for Application
	 * @param serverName
	 * @param appName
	 * @return
	 * @throws MalformedObjectNameException
	 */
	public static ObjectName getApplicationObjectName(String serverName, String appName) throws MalformedObjectNameException {
		return new ObjectName(
				"com.bea:ServerRuntime="+serverName+",Name="+appName+",Location="+serverName+",Type=ApplicationRuntime"
				);
	}
	
	public static ObjectName getComponentApplicationObjectName(String serverName, String appName, String componentName, String jmxType) throws MalformedObjectNameException {
		return new ObjectName(
				"com.bea:ApplicationRuntime="+appName+",Location="+serverName+",Name="+componentName+",ServerRuntime="+serverName+",Type="+jmxType
				);
	}
	
	
	public static String JvmType = "JVMRuntime";
	
	/**
	 * Get ObjectName for JVM
	 * @param serverName
	 * @param appName
	 * @return
	 * @throws MalformedObjectNameException
	 */
	public static ObjectName getJVMObjectName(String serverName) throws MalformedObjectNameException {
		return new ObjectName(
				"com.bea:ServerRuntime="+serverName+",Name="+serverName+",Location="+serverName+",Type=JVMRuntime"
				);
	}
	
	public static String ServerChannelType = "ServerChannelRuntime";
	
	public static ObjectName getServerChannelObjectName(String serverName, String channelName) throws MalformedObjectNameException {
		return new ObjectName(
				"com.bea:ServerRuntime="+serverName+",Name="+channelName+",Location="+serverName+",Type=ServerChannelRuntime"
				);
	}
	
	public static String ScaType = "??";
	
	public static ObjectName getSCACompositeObjectName(String serverName, String partition, String scaName, String revision) throws MalformedObjectNameException {
		return new ObjectName("oracle.soa.config:Location="+serverName+",partition="+partition+",j2eeType=SCAComposite,revision="+revision+",label=*,Application=soa-infra,wsconfigtype=WebServicesConfig,name="+scaName);
	}
	
	public static String OsbType = "ResourceMonitoringMBean";
	
	/**
	 * Get ObjectName of a OSB Resource
	 * @param serverName
	 * @param resourceName
	 * @return
	 * @throws MalformedObjectNameException
	 */
	public static ObjectName getOSBResourcesObjectName(String serverName, String resourceName) throws MalformedObjectNameException {
		return new ObjectName("com.oracle.osb:Name="+resourceName+",Location="+serverName+",Type=ResourceMonitoringMBean");
	}
	
	public static String WorkManagerType = "WorkManagerRuntime";
			
	/**
	 * Get ON of WorkManager
	 * @param serverName
	 * @param wmName
	 * @return
	 * @throws MalformedObjectNameException
	 */
	public static ObjectName getWorkManagerObjectName(String serverName, String wmName) throws MalformedObjectNameException {
		return new ObjectName("com.bea:ServerRuntime="+serverName+",Name="+wmName+",Location="+serverName+",Type=WorkManagerRuntime");
	}
	
	public static String BpelRequestType = "soainfra_bpel_requests";
	
	public static ObjectName getDMSBpelRequestObjectName(String serverName, String engineName) throws MalformedObjectNameException {
		return new ObjectName("oracle.dms:Location="+serverName+",name=/soainfra/engines/bpel/requests/"+engineName+",type=soainfra_bpel_requests");
	}
	
	public static String MessageProcessingType = "soainfra_message_processing";
	
	public static ObjectName getDMSMessageProcessingObjectName(String serverName, String msgProcessingName) throws MalformedObjectNameException {
		return new ObjectName("oracle.dms:Location="+serverName+",name=/soainfra/engines/message_processing/"+msgProcessingName+",type=soainfra_message_processing");
	}
	
	public static String CompositeType = "soainfra_composite";
	
	public static ObjectName getDMSCompositeObjectName(String serverName, String partitionName, String compositeName) throws MalformedObjectNameException {
		return new ObjectName("oracle.dms:Location="+serverName+",name=/soainfra/apps/"+partitionName+"/"+compositeName+",type=soainfra_composite");
	}

}
