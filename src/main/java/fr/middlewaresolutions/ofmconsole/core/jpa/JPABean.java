/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.core.jpa;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.api.MetricsService;

/**
 * Abstract bean for metrics
 * @author Emmanuel
 *
 */
public abstract class JPABean implements MetricsService {

	@EJB
	protected Administration admin;
	
	@PersistenceContext(unitName="metrics")
	protected EntityManager em;
	
	protected Logger LOG = Logger.getLogger(this.getClass().getName());
	
	/** Format de stockage */
	protected SimpleDateFormat fdm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	/**
	 * 
	 */
	public JPABean() {
		super();
	}

}
