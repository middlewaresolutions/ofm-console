package fr.middlewaresolutions.ofmconsole.core.jpa;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.management.ObjectName;

import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.metric.AbstractValue;
import fr.middlewaresolutions.ofmconsole.metric.Metric;
import fr.middlewaresolutions.ofmconsole.metric.MetricAttribute;
import fr.middlewaresolutions.ofmconsole.metric.NumericValue;
import fr.middlewaresolutions.ofmconsole.metric.Owner;
import fr.middlewaresolutions.ofmconsole.metric.TextValue;
import fr.middlewaresolutions.ofmconsole.model.Application;
import fr.middlewaresolutions.ofmconsole.model.Base;
import fr.middlewaresolutions.ofmconsole.model.SCAComposite;
import fr.middlewaresolutions.ofmconsole.performance.PerformanceTrace;

/**
 * Persist data with JPA
 * @author Emmanuel
 *
 */
@Stateless(name="PersistJPAMetricsBean", mappedName="PersistJPAMetricsBean")
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class PersistJPAMetricsBean extends JPABean implements PersistMetrics {
	
	
	
	public PersistJPAMetricsBean() {
		super();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveBase(Collection<? extends Base> jmxObjects) throws CockpitException {
		Calendar now = Calendar.getInstance();
		
		for(Base jmxObject: jmxObjects) {
			Metric metric = new Metric();
			
			// Time of metric ? defined or not ?
			if (jmxObject.getTime() == null)
				metric.setTime(now);
			else
				metric.setTime(jmxObject.getTime());
			
			metric.setName(jmxObject.getJmxObjectName());
			LOG.fine("persist: "+metric.getName());
			
			// Split JMX Name
			String[] jmx = jmxObject.getJmxObjectName().split(",");
			// Store only if filed is present
			for(String part: jmx) {
				if(part.toLowerCase().indexOf("location=")>=0)
					metric.setJmx_location(part.substring(part.indexOf("=")+1));
				else if(part.toLowerCase().indexOf("name=")>=0)
					metric.setJmx_name(part.substring(part.indexOf("=")+1));
				else if(part.toLowerCase().indexOf("serverruntime=")>=0)
					metric.setJmx_serverRuntime(part.substring(part.indexOf("=")+1));
				else if(part.toLowerCase().indexOf("type=")>=0)
					metric.setJmx_type(part.substring(part.indexOf("=")+1));
			}
			
			// clean JMX Info
			if (metric.getJmx_location() != null)
				metric.setJmx_location(metric.getJmx_location()
						.replaceAll("\"", ""));
			if (metric.getJmx_name() != null)
				metric.setJmx_name(metric.getJmx_name()
						.replaceAll("\"", ""));
			if (metric.getJmx_serverRuntime() != null)
				metric.setJmx_serverRuntime(metric.getJmx_serverRuntime()
						.replaceAll("\"", ""));
			if (metric.getJmx_type() != null)
				metric.setJmx_type(metric.getJmx_type()
						.replaceAll("\"", ""));

			// type numeric ?
			boolean isNumeric = true;
			
			// Store attributes
			for(String key: jmxObject.getAttributeKeys()) {
				Object value = jmxObject.getAttributeValue(key);
				isNumeric = true;
				
                //If an error occur, next attribute...
                try {
                    if ((value != null) && value.toString().length() >= AbstractValue.MaxLength) {
                        LOG.info("Key "+key+" not strored. Too long: "+value.toString().length());
                        continue;
                    }

//                    try {
//                    	if ((value != null) && !value.equals(""))
//                    		value = Double.valueOf((String) value);
//					} catch (Exception e) {
//						// nothing to do, it is a String !
//						LOG.fine("Key "+key+" is not a Double");
//					}
                    
                    // Add an atribute in function of his type
                    if (value instanceof String) {
                        metric.addTextAttribute(key, new TextValue(key, (String)value));
                        isNumeric = false;
                    } else if (value instanceof Integer)
                        metric.addNumericAttribute(key, new NumericValue(key, ((Integer)value).doubleValue()));
                    else if (value instanceof Double)
                        metric.addNumericAttribute(key, new NumericValue(key, ((Double)value)));
                    else if (value instanceof Long)
                        metric.addNumericAttribute(key, new NumericValue(key, ((Long)value).doubleValue()));
                    else if (value instanceof Float)
                        metric.addNumericAttribute(key, new NumericValue(key, ((Float)value).doubleValue()));
/*                    else if (value instanceof Map) {
                        // Store maps like attribute
                        for(Object keyMap: ((Map)value).keySet()) {
                            Object keyValue = ((Map)value).get(keyMap);
                            String keyString = keyMap.toString();

                            if (keyValue instanceof String)
                                metric.addTextAttribute(keyString, new TextValue(keyString, (String)keyValue));
                            else if (keyValue instanceof Integer)
                                metric.addNumericAttribute(keyString, new NumericValue(keyString, ((Integer)keyValue).doubleValue()));
                            else if (keyValue instanceof Double)
                                metric.addNumericAttribute(keyString, new NumericValue(keyString, ((Double)keyValue)));
                            else if (keyValue instanceof Long)
                                metric.addNumericAttribute(keyString, new NumericValue(keyString, ((Long)keyValue).doubleValue()));
                            else if (keyValue instanceof Float)
                                metric.addNumericAttribute(keyString, new NumericValue(keyString, ((Float)keyValue).doubleValue()));
                            else
                                metric.addTextAttribute(keyString, new TextValue(keyString, (keyValue !=null) ? keyValue.toString(): null));					}
                    }
*/                    
                    else {
                        // Impossible to use content: object, tables, etc
                        metric.addTextAttribute(key, new TextValue(key, (value !=null) ? value.toString(): null));
                        
                        isNumeric = false;
                    }

                    // persist trace of attribute
                    storeMetricAttribute(metric.getName(), metric.getJmx_name(), metric.getJmx_location(), metric.getJmx_serverRuntime(), 
                            metric.getJmx_type(), key, isNumeric);
                } catch (Exception e) {
                    LOG.info("Error during extraction of "+key+" in "+metric.getName()+": "+e.getMessage());
                }
			}
			
			// Remove ServerName and DomainName attribute. => properties of Owner
			String serverName = ((TextValue)metric.getValue(Infrastructure.ServerName)).getValue();
			String domainName = ((TextValue)metric.getValue(Infrastructure.DomainName)).getValue();
			
			metric.getTextAttributes().remove(metric.getValue(Infrastructure.ServerName));
			metric.getTextAttributes().remove(metric.getValue(Infrastructure.DomainName));
			
			// add owner of data
			Owner owner = getOwner(domainName, serverName);
			metric.setOwner(owner);
			
			em.persist(metric);
			
			// Store Component of Application also
			if (jmxObject instanceof Application)
				saveBase(((Application)jmxObject).getComponents());
			else if (jmxObject instanceof SCAComposite) {
				// Store References and Services of SCA
				saveBase(((SCAComposite)jmxObject).getReferences());
				saveBase(((SCAComposite)jmxObject).getServices());
			}
		}
	}
	
	@Override
	public List<Metric> lastMetricsOf(ObjectName objectName) throws CockpitException {
		String jmxName = objectName.getCanonicalName();
		
		Calendar max = (Calendar) em.createNamedQuery(Metric.MaxOfName)
			.setParameter("name", jmxName)
			.getSingleResult();
		
		if (max != null)
			return em.createNamedQuery(Metric.SelectByNameAndTime)
					.setParameter("name", jmxName)
					.setParameter("time", max)
					.getResultList();
		else
			return new ArrayList<Metric>();
		
	}
	
	@Override
	public List<Metric> lastMetricsOfWithJmxName(String partialObjectName) throws CockpitException {
		// Find max
		Calendar max = (Calendar) em.createNamedQuery(Metric.MaxOfName)
			.setParameter("name", partialObjectName)
			.getSingleResult();
		
		// Find object which have the max
		if (max != null)
			return em.createNamedQuery(Metric.SelectByNameAndTime)
					.setParameter("name", partialObjectName)
					.setParameter("time", max)
					.getResultList();
		else
			return new ArrayList<Metric>();
		
	}
	
	@Override
	public List<Metric> lastMetricsOfWithJmxType(String jmxType) throws CockpitException {
		// Find max
		Calendar max = (Calendar) em.createNamedQuery(Metric.MaxOfType)
			.setParameter("type", jmxType)
			.getSingleResult();
		
		// Find object which have the max
		if (max != null)
			return em.createNamedQuery(Metric.SelectByTypeAndTime)
					.setParameter("type", jmxType)
					.setParameter("time", max)
					.getResultList();
		else
			return new ArrayList<Metric>();
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void clear() throws CockpitException {
		em.clear();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storePerformance(String name, Calendar start, Calendar end) throws CockpitException {
		LOG.fine("Performance "+name+" ["+
				((start != null) ? fdm.format(start.getTime()) : "") 
				+","+
				((end != null) ? fdm.format(end.getTime()) : "")
				+"]");
		
		PerformanceTrace p = em.find(PerformanceTrace.class, name);
		if (p == null)
			p = new PerformanceTrace(name, start, end);
		else{
			p.setStart(start);
			p.setEnd(end);
		}
		
		em.merge(p);
	}
	
	/**
	 * Get Owner of data
	 * @param domain
	 * @param server
	 * @return
	 */
	@Override
	public Owner getOwner(String domain, String server) {
		List<Owner> ref = em.createQuery("select d from Owner d where d.domain = :domain and d.server = :server")
				.setParameter("domain", domain)
				.setParameter("server", server)
				.getResultList();
		
		Owner owner = null;
		
		if ((ref != null) && (ref.size() >0))
			owner = ref.get(0);
		else {
			owner = new Owner();
			owner.setDomain(domain);
			owner.setServer(server);
			
			em.persist(owner);
		}
		
		return owner;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void storeMetricAttribute(
			String jmxFull, String jmxName, String jmxLocation, 
			String jmxServerRuntime, String jmxType, String attributeName,
			boolean numeric) {
		List<MetricAttribute> mAttributes = em.createNamedQuery(MetricAttribute.FindByJmxFullAndAttributeName)
				.setParameter("jmxFull", jmxFull)
				.setParameter("attributeName", attributeName)
				.getResultList();
		
		MetricAttribute metricAttribute = null;
		
		if (mAttributes.size() >0)
			metricAttribute = mAttributes.get(0);
		else
			metricAttribute = new MetricAttribute();
		
		metricAttribute.setJmxFull(jmxFull);
		metricAttribute.setJmxLocation(jmxLocation);
		metricAttribute.setJmxName(jmxName);
		metricAttribute.setJmxServerRuntime(jmxServerRuntime);
		metricAttribute.setJmxType(jmxType);
		metricAttribute.setAttributeName(attributeName);
		
		// type: numeric or text ?
		metricAttribute.setNumeric(numeric);
		
		em.merge(metricAttribute);
	}
	
	@Override
	public List<MetricAttribute> listMetricAttributesByJmxFullName(String jmxFullName, boolean isNumeric) {
		return em.createNamedQuery(MetricAttribute.FindByJmxFull)
					.setParameter("jmxFull", jmxFullName)
					.setParameter("numeric", isNumeric)
					.getResultList();
	}
	
	@Override
	public List<String> listAttributesNamesOfJmxType(String jmxType) {
		List<Object[]> attrNames = em.createNamedQuery(MetricAttribute.FindAttributeNameByJmxType)
					.setParameter("jmxType", jmxType)
					.getResultList();
		
		List<String> attributes = new ArrayList<String>();
		for (Object[] att: attrNames)
			attributes.add((String)att[0]);
		
		return attributes;
	}

	@Override
	public List<NumericValue> listNumericValuesOfMetricByJmxType(String jmxType, String attribute, Calendar begin,
			Calendar end) {
		return em.createNamedQuery(NumericValue.FindByMetricJmxTypeAndNameInInterval)
				.setParameter("jmxType", jmxType)
				.setParameter("attributeName", attribute)
				.setParameter("start", begin)
				.setParameter("end", end)
				.getResultList();
	}
	
	@Override
	public List<NumericValue> listNumericValuesOfMetricByJmxType(String jmxType, Calendar begin, Calendar end) {
		return em.createNamedQuery(NumericValue.FindByMetricTypeInInterval)
				.setParameter("jmxType", jmxType)
				.setParameter("start", begin)
				.setParameter("end", end)
				.getResultList();
	}
	
	@Override
	public List<NumericValue> listNumericValuesOfMetricByJmxName(String jmxName, String attribute, Calendar begin, Calendar end) {
		return em.createNamedQuery(NumericValue.FindByMetricJmxNameAndNameInInterval)
				.setParameter("jmxName", jmxName)
				.setParameter("attributeName", attribute)
				.setParameter("start", begin)
				.setParameter("end", end)
				.getResultList();
	}

	@Override
	public List<MetricAttribute> listMetricAttributesByJmxType(String jmxType, boolean isNumeric) {
		return em.createNamedQuery(MetricAttribute.FindByJmxTypeAndNumeric)
				.setParameter("jmxType", jmxType)
				.setParameter("numeric", isNumeric)
				.getResultList();
	}
	
	/**
	 * Return a simple instance from his type and PK
	 * @param classe
	 * @param primaryKey
	 * @return
	 */
	@Override
	public <T> T find(Class<T> classe, Object primaryKey) {
		return em.find(classe, primaryKey);
	}
}
