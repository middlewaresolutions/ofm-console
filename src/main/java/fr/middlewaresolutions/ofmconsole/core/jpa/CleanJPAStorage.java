/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.core.jpa;

import java.util.Calendar;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import fr.middlewaresolutions.ofmconsole.api.CleanStorage;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;

/**
 * Delete Metrics 
 * @author Emmanuel
 *
 */
@Stateless(name="CleanJPAStorage", mappedName="CleanJPAStorage")
public class CleanJPAStorage extends JPABean implements CleanStorage {
	
	@EJB
	private PersistMetrics store;
	
	/**
	 * 
	 */
	public CleanJPAStorage() {
		super();
	}

	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.api.CleanStorage#cleanMetric(java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public void cleanMetric(Calendar start, Calendar end) throws CockpitException {
		
		int deleted = em.createQuery("delete from Metric m where m.time between :start and :end")
				.setParameter("start", start)
				.setParameter("end", end)
				.executeUpdate();
		
		LOG.info("Metric deleted ["+fdm.format(start.getTime())+","+fdm.format(start.getTime())+"]: "+deleted);
	}

	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.api.CleanStorage#cleanMetricPer5Min(java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public void cleanMetricPer5Min(Calendar start, Calendar end) throws CockpitException {
		
		int deleted = em.createQuery("delete from MetricPer5Min m where m.time between :start and :end")
				.setParameter("start", start)
				.setParameter("end", end)
				.executeUpdate();
		
		LOG.info("MetricPer5Min deleted ["+fdm.format(start.getTime())+","+fdm.format(start.getTime())+"]: "+deleted);
	}

	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.api.CleanStorage#cleanMetricPer15Min(java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public void cleanMetricPer15Min(Calendar start, Calendar end) throws CockpitException {
		
		int deleted = em.createQuery("delete from MetricPer15Min m where m.time between :start and :end")
				.setParameter("start", start)
				.setParameter("end", end)
				.executeUpdate();
		
		LOG.info("MetricPer15Min deleted ["+fdm.format(start.getTime())+","+fdm.format(start.getTime())+"]: "+deleted);

	}

	/* (non-Javadoc)
	 * @see fr.middlewaresolutions.ofmconsole.api.CleanStorage#cleanMetricPerHour(java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public void cleanMetricPerHour(Calendar start, Calendar end) throws CockpitException {
		
		int deleted = em.createQuery("delete from MetricPerHour m where m.time between :start and :end")
				.setParameter("start", start)
				.setParameter("end", end)
				.executeUpdate();
		
		LOG.info("MetricPerHour deleted ["+fdm.format(start.getTime())+","+fdm.format(start.getTime())+"]: "+deleted);
	}

}
