/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ejb.PrePassivate;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.IntrospectionException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.model.Application;
import fr.middlewaresolutions.ofmconsole.model.Base;
import fr.middlewaresolutions.ofmconsole.model.Cluster;
import fr.middlewaresolutions.ofmconsole.model.ComponentRuntime;
import fr.middlewaresolutions.ofmconsole.model.DMSMetric;
import fr.middlewaresolutions.ofmconsole.model.DataSource;
import fr.middlewaresolutions.ofmconsole.model.JMSDestination;
import fr.middlewaresolutions.ofmconsole.model.JVM;
import fr.middlewaresolutions.ofmconsole.model.OSBResource;
import fr.middlewaresolutions.ofmconsole.model.SCABinding;
import fr.middlewaresolutions.ofmconsole.model.SCAComponent;
import fr.middlewaresolutions.ofmconsole.model.SCAComposite;
import fr.middlewaresolutions.ofmconsole.model.Server;
import fr.middlewaresolutions.ofmconsole.model.ServerChannel;
import fr.middlewaresolutions.ofmconsole.model.ThreadConstraint;
import fr.middlewaresolutions.ofmconsole.model.WorkManager;

/**
 * Scan an Weblogic Infrastructure
 * @author Emmanuel
 *
 */
@Stateless(name="InfrastructureBean", mappedName="InfrastructureBean")
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class InfrastructureBean implements Infrastructure {

	MBeanServerConnection mBeanDomainServer;
	MBeanServerConnection mBeanDomainServerRemote;
	JMXConnector jmxConnector;
	
//	MBeanServer mBeanRuntimeServer;
	Logger LOG = Logger.getLogger(this.getClass().getName());
	
	public InfrastructureBean() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * Init local ressources
	 * @param username
	 * @param password
	 * @throws IOException
	 * @throws NamingException
	 */
	protected void initLocalConnection(String username, String password) throws IOException, NamingException {

		Hashtable h = new Hashtable();
		h.put(Context.SECURITY_PRINCIPAL, username);
		h.put(Context.SECURITY_CREDENTIALS, password);

		InitialContext ctx = new InitialContext(h);
//		mBeanRuntimeServer = (MBeanServer) ctx.lookup("java:comp/env/jmx/runtime");
		mBeanDomainServer = (MBeanServerConnection) ctx.lookup("java:comp/env/jmx/domainRuntime");

		if (mBeanDomainServerRemote == null)
			mBeanDomainServerRemote = mBeanDomainServer;
	}
	
	/**
	 * Init JNDI Remote connection
	 * @param hostname
	 * @param portString
	 * @param username
	 * @param password
	 * @throws IOException
	 */
	protected void initRemoteConnection(String hostname, String portString, String username, String password) throws IOException {
		
		// Create Connector if necessary
		if (jmxConnector == null) {
			Hashtable h = new Hashtable();
			h.put(Context.SECURITY_PRINCIPAL, username);
			h.put(Context.SECURITY_CREDENTIALS, password);
			h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");
			h.put("jmx.remote.x.request.waiting.timeout", new Long(30000));
	
			String protocol = "t3";
			Integer portInteger = Integer.valueOf(portString);
			int port = portInteger.intValue();
			String jndiroot = "/jndi/";
			String mserver = "weblogic.management.mbeanservers.domainruntime";
			JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostname, port, jndiroot + mserver);
	
			jmxConnector = JMXConnectorFactory.connect(serviceURL, h);
		}
		
		// Get JMX Connection
		mBeanDomainServerRemote = jmxConnector.getMBeanServerConnection();
		
		if (mBeanDomainServer == null)
			mBeanDomainServer = mBeanDomainServerRemote;
	}

	protected void initRemoteConnection(Domain domain) throws IOException {
		initRemoteConnection(domain.getHostname(), domain.getPort(), domain.getUsername(), domain.getPassword());
	}
	
	@PrePassivate
	public void closeRemoteConnection() {
		try {
			if (jmxConnector != null) {
				jmxConnector.close();
				LOG.info("JMX connection closed");
			} else
				LOG.info("JMX connection yet closed");
		} catch (IOException e) {
			LOG.warning("Impossible to close JMX connection. Cause: "+e.getMessage());
		}
	}
	/**
	 * Extract runtime information for a server
	 * @param onServer
	 * @return
	 * @throws AttributeNotFoundException
	 * @throws InstanceNotFoundException
	 * @throws MBeanException
	 * @throws ReflectionException
	 * @throws IntrospectionException 
	 * @throws IOException 
	 */
	private void extractAllAttributes(ObjectName on, Base instance) throws IntrospectionException, InstanceNotFoundException, ReflectionException, IOException 
    {
		extractAllAttributes(mBeanDomainServer, on, instance);
    }
	
	
	private Map<String, Object> toMap(CompositeData data)
	{
	    HashMap<String, Object> builder = new HashMap<String, Object>();

	    // never trust JMX to do the right thing
	    Set<String> keySet = data.getCompositeType().keySet();
	    if (keySet != null) {
	        for (String key : keySet) {
	            if (key != null) {
	            	LOG.fine("Parse "+key);
	            	
	                Object value = data.get(key);
	                if (value != null)
	                	// Inclus un TabularDataSupport ?
	                	if (value instanceof TabularDataSupport) {
	                		
	                		// Multiple lignes à ce TabularDataSupport
	                		ArrayList<Object> tab = new ArrayList<Object>();
	                		builder.put(key, tab);
	                		
	                		// Chaque ligne est ajouté dans la List regroupant les N valeurs
	                		for(Object row: ((TabularDataSupport)value).values()) {

	                			// Extraction en Map pour le CompositeData
	                			if (row instanceof CompositeData)
	                				tab.add(toMap((CompositeData)row));
	                			else
	                				tab.add(row);
	                		}
	                	} else if (value instanceof CompositeData)
	                		builder.put(key, toMap((CompositeData)value));
	                	else
	                		builder.put(key, value);
	                
	            }
	        }
	    }
	
	    return builder;
	}
	
	/**
	 * Extract mbean informations
	 * @param mbeanServer
	 * @param on
	 * @param instance
	 * @throws IntrospectionException
	 * @throws InstanceNotFoundException
	 * @throws ReflectionException
	 * @throws IOException
	 */
	private void extractAllAttributes(MBeanServerConnection mbeanServer, ObjectName on, Base instance) throws IntrospectionException, InstanceNotFoundException, ReflectionException, IOException 
    {

		LOG.fine("Extract attributes of "+ on.getCanonicalName());
		instance.setJmxObjectName(on.getCanonicalName());

		// Parse all attributes
		MBeanInfo infos = mbeanServer.getMBeanInfo(on);
		for (MBeanAttributeInfo attrInfo : infos.getAttributes()) {
			if (attrInfo != null)
				try {
					Object value = mbeanServer.getAttribute(on, attrInfo.getName());
					
					if (value instanceof CompositeData) {
						instance.addAttribute(attrInfo.getName(), toMap((CompositeData)value));
					} else 
						instance.addAttribute(attrInfo.getName(), value);
				} catch (Exception e) {
					LOG.warning("Attribute value inaccessible: "+attrInfo.getName());
				}
		}

	}
	
	/**
	 * Nom du serveur d'administration
	 * @return
	 * @throws ReflectionException 
	 * @throws MBeanException 
	 * @throws MalformedObjectNameException 
	 * @throws InstanceNotFoundException 
	 * @throws AttributeNotFoundException 
	 * @throws IOException 
	 */
	public String getAdminServerName() 
	throws AttributeNotFoundException, InstanceNotFoundException, MalformedObjectNameException, MBeanException, ReflectionException, IOException {
		return (String) mBeanDomainServer.getAttribute(
				ObjectNameFactory.getDomainObjectName(getDomainName()), 
				"AdminServerName");
	}
	
	
	/**
	 * List all servers form Domain
	 * @throws IOException 
	 */
	@Override
	public List<Server> listDomainServers(Domain domain) throws IOException {

		// jmx connect
		initRemoteConnection(domain);
				
		ArrayList<Server> servers = new ArrayList<Server>();

		try {
			// ON des Servers
			ObjectName[] onServers = (ObjectName[]) mBeanDomainServer
					.getAttribute(ObjectNameFactory.getDomainObjectName(getDomainName()), "Servers");

			// Parcours des Serveurs du domaine
			for (ObjectName onServer : onServers) {

				// retreive server name
				String serverName = (String) mBeanDomainServer.getAttribute(onServer, "Name");
				
				// Create server
				Server wlsServer = new Server();
				extractAllAttributes(ObjectNameFactory.getServerObjectName(serverName), wlsServer);
				
				// Add attribute to know owner
				wlsServer.addAttribute(Infrastructure.DomainName, getDomainName());
				wlsServer.addAttribute(Infrastructure.ServerName, serverName);
				
				servers.add(wlsServer);

			}

		} catch (MalformedObjectNameException e) {
			LOG.warning(e.getMessage());
			
		} catch (InstanceNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (AttributeNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (ReflectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (MBeanException e) {
			LOG.warning(e.getMessage());
			
		} catch (IntrospectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (IOException e) {
			LOG.warning(e.getMessage());
			
		}

		return servers;
	}

	/**
	 * Nom du domaine WLS
	 * @throws IOException 
	 */
	public String getDomainName() throws IOException {
		return mBeanDomainServer.getDefaultDomain();
	}
	
	/**
	 * Liste les clusters WLS
	 * @throws IOException 
	 */
	@Override
	public List<Cluster> listDomainClusters(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);

		ArrayList<Cluster> clusters = new ArrayList<Cluster>();

		try {
			// ON des Clusters
			ObjectName[] onClusters = (ObjectName[]) mBeanDomainServer
					.getAttribute(ObjectNameFactory.getDomainObjectName(getDomainName()), "Clusters");

			for (ObjectName onCluster : onClusters) {
				
				Cluster wlsCluster = new Cluster();
				clusters.add(wlsCluster);
				extractAllAttributes(onCluster, wlsCluster);
				
				// ON des Servers
				ObjectName[] onServers = (ObjectName[]) mBeanDomainServer.getAttribute(onCluster, "Servers");
			
				// Parcours des Serveurs du domaine
				for (ObjectName onServer : onServers) {
					String name = (String) mBeanDomainServer.getAttribute(onServer, "Name");
					
					Server wlsServer = new Server();
					extractAllAttributes(ObjectNameFactory.getServerObjectName(name), wlsServer);
					
					// Add attribute to know owner
					wlsServer.addAttribute(Infrastructure.DomainName, getDomainName());
					wlsServer.addAttribute(Infrastructure.ServerName, name);
					
					wlsCluster.getServers().add(wlsServer);

				}
			}

		} catch (MalformedObjectNameException e) {
			LOG.warning(e.getMessage());
			
		} catch (InstanceNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (AttributeNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (ReflectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (MBeanException e) {
			LOG.warning(e.getMessage());
			
		} catch (IntrospectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (IOException e) {
			LOG.warning(e.getMessage());
			
		}

		return clusters;
	}

	/**
	 * List all datasources
	 * @return
	 * @throws IOException 
	 */
	@Override
	public List<DataSource> listDomainDataSources(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);
				
		ArrayList<DataSource> dataSources = new ArrayList<DataSource>();

		try {
			for(Server server: listDomainServers(domain)) {
				// ON des Clusters
				Set<ObjectInstance> oiDataSources = mBeanDomainServer
						.queryMBeans(ObjectNameFactory.getDataSourceObjectName(server.getName(), "*"), null);
		
				for (ObjectInstance oiDS : oiDataSources) {
					
					DataSource ds = new DataSource();
					extractAllAttributes(oiDS.getObjectName(), ds);
					
					// Add attribute to know owner
					ds.addAttribute(Infrastructure.DomainName, getDomainName());
					ds.addAttribute(Infrastructure.ServerName, server.getName());
					
					dataSources.add(ds);
					
				}
			}
	
		} catch (MalformedObjectNameException | InstanceNotFoundException | ReflectionException | IntrospectionException | IOException e) {
			LOG.warning(e.getMessage());
			
		}
	
		return dataSources;
	}
	
	/**
	 * List all JMSDestinations
	 * @return
	 * @throws IOException 
	 */
	@Override
	public List<JMSDestination> listDomainJMS(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);
		
		ArrayList<JMSDestination> jmsDestination = new ArrayList<JMSDestination>();

		try {
			for(Server server: listDomainServers(domain)) {
				// ON des Clusters
				Set<ObjectInstance> oiJMSServers = mBeanDomainServer
						.queryMBeans(ObjectNameFactory.getJMSServerObjectName(server.getName(), "*"), null);
		
				// Pour chaque serveur JMS déployé
				for (ObjectInstance oiJMSServer : oiJMSServers) {
					
					ObjectName[] onJMSDestinations = (ObjectName[]) mBeanDomainServer.getAttribute(oiJMSServer.getObjectName(), "Destinations");
					
					// Parcours des Destinations JMS
					for (ObjectName onJMSDestination : onJMSDestinations) {
					
						JMSDestination dest = new JMSDestination();
						extractAllAttributes(onJMSDestination, dest);
					
						// Add attribute to know owner
						dest.addAttribute(Infrastructure.DomainName, getDomainName());
						dest.addAttribute(Infrastructure.ServerName, server.getName());
						
						jmsDestination.add(dest);
					}
				}
			}
	
		} catch (MalformedObjectNameException | InstanceNotFoundException | ReflectionException | IntrospectionException | AttributeNotFoundException | MBeanException | IOException e) {
			LOG.warning("Impossible to parse JMS Destinations. \nCause: "+e.getMessage());
		}
	
		return jmsDestination;
	}
	
	@Override
	public List<Application> listDomainApplications(String TypeFilter, Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);		
		
		ArrayList<Application> applications = new ArrayList<Application>();

		try {
			for(Server server: listDomainServers(domain)) {
				// ON des Applications
				Set<ObjectInstance> oiApplications = mBeanDomainServer
						.queryMBeans(ObjectNameFactory.getApplicationObjectName(server.getName(), "*"), null);
		
				// Pour chaque application déployée
				for (ObjectInstance oiApplication : oiApplications) {
					
					ObjectName[] onComponentRuntimes = (ObjectName[]) mBeanDomainServer.getAttribute(oiApplication.getObjectName(), "ComponentRuntimes");
					
					// Extract application attributes
					Application app = new Application();
					extractAllAttributes(oiApplication.getObjectName(), app);
					applications.add(app);
					
					// Add attribute to know owner
					app.addAttribute(Infrastructure.DomainName, getDomainName());
					app.addAttribute(Infrastructure.ServerName, server.getName());
					
					// Parcours des Components
					for (ObjectName onComponentRuntime : onComponentRuntimes) {
					
						ComponentRuntime compo = new ComponentRuntime();
						extractAllAttributes(onComponentRuntime, compo);
					
						// Add attribute to know owner
						compo.addAttribute(Infrastructure.DomainName, getDomainName());
						compo.addAttribute(Infrastructure.ServerName, server.getName());
						
						if ((TypeFilter == null) ||
							(TypeFilter != null) && TypeFilter.equals(compo.getAttributeValue("Type"))) 
							// attach to application
							app.addComponents(compo);
						
					}
				}
			}
	
		} catch (MalformedObjectNameException | InstanceNotFoundException | ReflectionException | IntrospectionException | AttributeNotFoundException | MBeanException | IOException e) {
			LOG.warning("Impossible to parse Applications. \nCause: "+e.getMessage());
		}
	
		return applications;
	}
	
	@Override
	public List<JVM> listDomainJVM(Domain domain) throws IOException {

		// jmx connect
		initRemoteConnection(domain);

		
		ArrayList<JVM> jvms = new ArrayList<JVM>();

		try {
			// ON des Servers
			ObjectName[] onServers = (ObjectName[]) mBeanDomainServer
					.getAttribute(ObjectNameFactory.getDomainObjectName(getDomainName()), "Servers");

			// Parcours des Serveurs du domaine
			for (ObjectName onServer : onServers) {

				// retreive server name
				String serverName = (String) mBeanDomainServer.getAttribute(onServer, "Name");
				
				// Create server
				JVM jvm = new JVM();
				extractAllAttributes(ObjectNameFactory.getJVMObjectName(serverName), jvm);
				
				// Add attribute to know owner
				jvm.addAttribute(Infrastructure.DomainName, getDomainName());
				jvm.addAttribute(Infrastructure.ServerName, serverName);
				
				jvms.add(jvm);

			}

		} catch (MalformedObjectNameException e) {
			LOG.warning(e.getMessage());
			
		} catch (InstanceNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (AttributeNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (ReflectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (MBeanException e) {
			LOG.warning(e.getMessage());
			
		} catch (IntrospectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (IOException e) {
			LOG.warning(e.getMessage());
			
		}

		return jvms;
	}
	
	@Override
	public List<ServerChannel> listDomainServerChannelRuntimes(String filterChannels, Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);
						
		ArrayList<ServerChannel> channels = new ArrayList<ServerChannel>();

		try {
			// ON des Servers
			ObjectName[] onServers = (ObjectName[]) mBeanDomainServer
					.getAttribute(ObjectNameFactory.getDomainObjectName(getDomainName()), "Servers");

			// Parcours des Serveurs du domaine
			for (ObjectName onServer : onServers) {

				// retreive server name
				String serverName = (String) mBeanDomainServer.getAttribute(onServer, "Name");
				
				// retreive server channels
				ObjectName[] onChannels = (ObjectName[]) mBeanDomainServer.getAttribute(ObjectNameFactory.getServerObjectName(serverName), "ServerChannelRuntimes");
				
				for (ObjectName channel : onChannels) {
					// Create server
					ServerChannel sc = new ServerChannel();
					extractAllAttributes(channel, sc);
					
					// Add attribute to know owner
					sc.addAttribute(Infrastructure.DomainName, getDomainName());
					sc.addAttribute(Infrastructure.ServerName, serverName);
					
					// filter channel
					if ((filterChannels==null) ||
						(filterChannels!=null && sc.getName().contains(filterChannels)))
						channels.add(sc);
				}

			}

		} catch (MalformedObjectNameException e) {
			LOG.warning(e.getMessage());
			
		} catch (InstanceNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (AttributeNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (ReflectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (MBeanException e) {
			LOG.warning(e.getMessage());
			
		} catch (IntrospectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (IOException e) {
			LOG.warning(e.getMessage());
			
		}

		return channels;
	}
	
	@Override
	public List<SCAComposite> listDomainSCAComposite(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);
				
				
		ArrayList<SCAComposite> composites = new ArrayList<SCAComposite>();

		try {
			// ON des Servers
			ObjectName[] onServers = (ObjectName[]) mBeanDomainServer
					.getAttribute(ObjectNameFactory.getDomainObjectName(getDomainName()), "Servers");

			// Parcours des Serveurs du domaine
			for (ObjectName onServer : onServers) {

				// retreive server name
				String serverName = (String) mBeanDomainServer.getAttribute(onServer, "Name");
				
				// retreive sca
				// ON des Clusters
				Set<ObjectInstance> oiSCAComposites = mBeanDomainServerRemote
						.queryMBeans(ObjectNameFactory.getSCACompositeObjectName(serverName, "*", "*", "*"), null);
								
				for (ObjectInstance oiComposite: oiSCAComposites) {
					
					try {
						// Create server
						SCAComposite sca = new SCAComposite();
						extractAllAttributes(oiComposite.getObjectName(), sca);
						composites.add(sca);
						
						// Add attribute to know owner
						sca.addAttribute(Infrastructure.DomainName, getDomainName());
						sca.addAttribute(Infrastructure.ServerName, serverName);
						
						// Components
						ObjectName[] onComponents = (ObjectName[]) mBeanDomainServerRemote.getAttribute(oiComposite.getObjectName(), "Components");
						for (ObjectName onComponent : onComponents) {

							SCAComponent compo = new SCAComponent();
							extractAllAttributes(mBeanDomainServerRemote, onComponent, compo);
							sca.addComponent(compo);
						}
						
						// Services	
						ObjectName[] onServices = (ObjectName[]) mBeanDomainServerRemote.getAttribute(oiComposite.getObjectName(), "Services");
						for (ObjectName onService : onServices) {
							
							// Bindings
							ObjectName[] onBindings = (ObjectName[]) mBeanDomainServerRemote.getAttribute(onService, "Bindings");
							for (ObjectName onBinding : onBindings) {	
								SCABinding service = new SCABinding();
								extractAllAttributes(mBeanDomainServerRemote, onBinding, service);
								sca.addService(service);
                                
                                // Add attribute to know owner
                                service.addAttribute(Infrastructure.DomainName, getDomainName());
                                service.addAttribute(Infrastructure.ServerName, serverName);
                                
							}
						}
						
						// References	
						ObjectName[] onReferences = (ObjectName[]) mBeanDomainServerRemote.getAttribute(oiComposite.getObjectName(), "References");
						for (ObjectName onReference : onReferences) {
							
							// Bindings
							ObjectName[] onBindings = (ObjectName[]) mBeanDomainServerRemote.getAttribute(onReference, "Bindings");
							for (ObjectName onBinding : onBindings) {	
								SCABinding ref = new SCABinding();
								extractAllAttributes(mBeanDomainServerRemote, onBinding, ref);
								sca.addReference(ref);
                                
                                // Add attribute to know owner
                                ref.addAttribute(Infrastructure.DomainName, getDomainName());
                                ref.addAttribute(Infrastructure.ServerName, serverName);
							}
						}
					} catch (Exception e) {
						LOG.warning(e.getMessage());
						
					}
				}

			}

		} catch (Exception e) {
			LOG.warning(e.getMessage());
			
		}

		return composites;
	}
	
	@Override
	public Collection<WorkManager> listDomainWorkManager(Domain domain) throws IOException {

		// jmx connect
		initRemoteConnection(domain);
		
		HashMap<String, WorkManager> wms = new HashMap<String, WorkManager>();

		try {
			// ON des Servers
			ObjectName[] onServers = (ObjectName[]) mBeanDomainServer
					.getAttribute(ObjectNameFactory.getDomainObjectName(getDomainName()), "Servers");

			// Parcours des Serveurs du domaine
			for (ObjectName onServer : onServers) {

				// retreive server name
				String serverName = (String) mBeanDomainServer.getAttribute(onServer, "Name");
				
				// retreive wm
				Set<ObjectInstance> oiWorkManagers = mBeanDomainServerRemote
						.queryMBeans(ObjectNameFactory.getWorkManagerObjectName(serverName, "*"), null);
								
				for (ObjectInstance oiWorkManager: oiWorkManagers) {
					String wmName = (String) mBeanDomainServer.getAttribute(oiWorkManager.getObjectName(), "Name");
					if (!wms.containsKey(wmName)) {
						// Create wm
						WorkManager wm = new WorkManager();
						extractAllAttributes(oiWorkManager.getObjectName(), wm);
	
						wms.put(wmName, wm);
						
						// Max defined ?
						String refMax = "MaxThreadsConstraintRuntime";
						Object onMax = mBeanDomainServer.getAttribute(oiWorkManager.getObjectName(),refMax);
						if (onMax != null) {
							ThreadConstraint tc = new ThreadConstraint();
							extractAllAttributes((ObjectName)onMax, tc);
							wm.setMax(tc);
							
							for(String maxKey: tc.getAttributeKeys()) {
								wm.addAttribute(refMax+"-"+maxKey, tc.getAttributeValue(maxKey));
							}
						}
						
						// Min defined ?
						String refMin = "MinThreadsConstraintRuntime";
						Object onMin = mBeanDomainServer.getAttribute(oiWorkManager.getObjectName(), "MinThreadsConstraintRuntime");
						if (onMin != null) {
							ThreadConstraint tc = new ThreadConstraint();
							extractAllAttributes((ObjectName)onMin, tc);
							wm.setMin(tc);
							
							for(String minKey: tc.getAttributeKeys()) {
								wm.addAttribute(refMin+"-"+minKey, tc.getAttributeValue(minKey));
							}
						}
						
						// Add attribute to know owner
						wm.addAttribute(Infrastructure.DomainName, getDomainName());
						wm.addAttribute(Infrastructure.ServerName, serverName);
					}
				}
			}

		} catch (MalformedObjectNameException e) {
			LOG.warning(e.getMessage());
			
		} catch (InstanceNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (AttributeNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (ReflectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (MBeanException e) {
			LOG.warning(e.getMessage());
			
		} catch (IntrospectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (IOException e) {
			LOG.warning(e.getMessage());
			
		}

		return wms.values();
	}
	
	@Override
	public List<DMSMetric> listDomainBpelEngine(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);

		ArrayList<DMSMetric> metrics = new ArrayList<DMSMetric>();

		try {
			// ON des Servers
			ObjectName[] onServers = (ObjectName[]) mBeanDomainServer
					.getAttribute(ObjectNameFactory.getDomainObjectName(getDomainName()), "Servers");

			// Parcours des Serveurs du domaine
			for (ObjectName onServer : onServers) {

				// retreive server name
				String serverName = (String) mBeanDomainServer.getAttribute(onServer, "Name");
				
				// Get engine
				Set<ObjectInstance> oiEngines = mBeanDomainServerRemote
						.queryMBeans(ObjectNameFactory.getDMSBpelRequestObjectName(serverName, "*"), null);
				
				for(ObjectInstance oi: oiEngines) {
					// Create server
					DMSMetric metric = new DMSMetric();
					extractAllAttributes(oi.getObjectName(), metric);
					
					// Add attribute to know owner
					metric.addAttribute(Infrastructure.DomainName, getDomainName());
					metric.addAttribute(Infrastructure.ServerName, serverName);
					
					metrics.add(metric);
				} 

			}

		} catch (MalformedObjectNameException e) {
			LOG.warning(e.getMessage());

		} catch (InstanceNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (AttributeNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (ReflectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (MBeanException e) {
			LOG.warning(e.getMessage());
			
		} catch (IntrospectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (IOException e) {
			LOG.warning(e.getMessage());
			
		}

		return metrics;
	}
	
	@Override
	public List<DMSMetric> listDomainMessageProcessing(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);

		ArrayList<DMSMetric> metrics = new ArrayList<DMSMetric>();

		try {
			// ON des Servers
			ObjectName[] onServers = (ObjectName[]) mBeanDomainServer
					.getAttribute(ObjectNameFactory.getDomainObjectName(getDomainName()), "Servers");

			// Parcours des Serveurs du domaine
			for (ObjectName onServer : onServers) {

				// retreive server name
				String serverName = (String) mBeanDomainServer.getAttribute(onServer, "Name");
				
				// Get engine
				Set<ObjectInstance> oiEngines = mBeanDomainServerRemote
						.queryMBeans(ObjectNameFactory.getDMSMessageProcessingObjectName(serverName, "*"), null);
				
				for(ObjectInstance oi: oiEngines) {
					// Create server
					DMSMetric metric = new DMSMetric();
					extractAllAttributes(oi.getObjectName(), metric);
					
					// Add attribute to know owner
					metric.addAttribute(Infrastructure.DomainName, getDomainName());
					metric.addAttribute(Infrastructure.ServerName, serverName);
					
					metrics.add(metric);
				}

			}

		} catch (MalformedObjectNameException e) {
			LOG.warning(e.getMessage());

		} catch (InstanceNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (AttributeNotFoundException e) {
			LOG.warning(e.getMessage());
			
		} catch (ReflectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (MBeanException e) {
			LOG.warning(e.getMessage());
			
		} catch (IntrospectionException e) {
			LOG.warning(e.getMessage());
			
		} catch (IOException e) {
			LOG.warning(e.getMessage());
			
		}

		return metrics;
	}
	
	
	@Override
	public List<OSBResource> listDomainOSBResources(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);

		ArrayList<OSBResource> ressources = new ArrayList<OSBResource>();

		try {

			// Parcours des Serveurs du domaine
			for (Server server: listDomainServers(domain)) {
				
				// retreive sca
				// ON des Clusters
				Set<ObjectInstance> oiResources = mBeanDomainServerRemote
						.queryMBeans(ObjectNameFactory.getOSBResourcesObjectName(server.getName(), "*"), null);
								
				for (ObjectInstance oiComposite: oiResources) {
						// Create resource
						OSBResource resource = new OSBResource();
						extractAllAttributes(oiComposite.getObjectName(), resource);
						ressources.add(resource);
						
						// Add attribute to know owner
						resource.addAttribute(Infrastructure.DomainName, getDomainName());
						resource.addAttribute(Infrastructure.ServerName, server.getName());
						
						// Split Name
						String name = resource.getName()+"";
						String[] nameSplit = name.replace('$', ':').split(":");
						if (nameSplit.length == 3) {
							resource.addAttribute("Type", nameSplit[0]);
							resource.addAttribute("Project", nameSplit[1]);
							resource.addAttribute("ResourceName", nameSplit[2]);
						} else
							LOG.severe("Impossible to split Name: "+resource.getName());
				}

			}

		} catch (Exception e) {
			LOG.warning(e.getMessage());
			
		}

		return ressources;
	}

	@Override
	public List<OSBResource> listDomainOSBResourcesWithStatistics(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);

		
		List<OSBResource> osbResources = listDomainOSBResources(domain);
		
		// Parse statistics
		for(OSBResource resource: osbResources) {
			HashMap<String, Object> metricsOfResource = new HashMap<String, Object>();
			resource.addAttribute("metrics", metricsOfResource);
			
			// Statistics are enable ?
			if (resource.getAttributeValue("Statistics") != null) {
				Map<String, Object> statsByResource = (Map<String, Object>) resource.getAttributeValue("Statistics");
				List<Object> serverStatistics = (List<Object>) statsByResource.get("server-statistics");
				
				// For each server
				for(Object serverStatistic: serverStatistics) {
					Map<String, Object> statsByServer = (Map<String, Object>) serverStatistic;
					
					// add server name of metrics
					String serverName = (String) statsByServer.get("server-name");
					
					List<Object> statsByMetric = (List<Object>) statsByServer.get("statistics");
					for(Object metric: statsByMetric) {
						Map<String, Object> metricByServerAndResource = (Map<String, Object>) metric;
						
						Map<String, Object> statLastAi = (Map<String, Object>) metricByServerAndResource.get("stat-last-ai");
						
						metricsOfResource.put((String) metricByServerAndResource.get("stat-name"), ((Object[])statLastAi.get("components"))[0]);
					}
				}
			}
			
			
		}
	
		return osbResources;
	}
	
	@Override
	public List<OSBResource> listDomainOSBStatistics(Domain domain) throws IOException {
		// jmx connect
		initRemoteConnection(domain);

		
		List<OSBResource> osbResources = listDomainOSBResources(domain);
		
		List<OSBResource> newResources = new ArrayList<OSBResource>();
		
		// Parse statistics
		for(OSBResource resource: osbResources) {
			HashMap<String, Object> metricsOfResource = new HashMap<String, Object>();
			resource.addAttribute("metrics", metricsOfResource);
			
			// Statistics are enable ?
			if (resource.getAttributeValue("Statistics") != null) {
				Map<String, Object> statsByResource = (Map<String, Object>) resource.getAttributeValue("Statistics");
				List<Object> serverStatistics = (List<Object>) statsByResource.get("server-statistics");
				
				// For each server
				for(Object serverStatistic: serverStatistics) {
					Map<String, Object> statsByServer = (Map<String, Object>) serverStatistic;
					
					// add server name of metrics
					String serverName = (String) statsByServer.get("server-name");
					
					// Create new Structure
					OSBResource osb = new OSBResource();
					newResources.add(osb);
					osb.setServerName(serverName);
					osb.setJmxObjectName(resource.getJmxObjectName().replaceAll(ObjectNameFactory.OsbType, ObjectNameFactory.OsbStatisticType));
					
					osb.addAttribute("Name", resource.getAttributeValue("Name"));
					
					// Specify value for this server
					osb.addAttribute(Infrastructure.ServerName, serverName);
					osb.addAttribute(Infrastructure.DomainName, resource.getAttributeValue(Infrastructure.DomainName));
					
					List<Object> statsByMetric = (List<Object>) statsByServer.get("statistics");
					for(Object metric: statsByMetric) {
						Map<String, Object> metricByServerAndResource = (Map<String, Object>) metric;
						
						Map<String, Object> statLastAi = (Map<String, Object>) metricByServerAndResource.get("stat-last-ai");
						
						osb.addAttribute((String) metricByServerAndResource.get("stat-name"), ((Object[])statLastAi.get("components"))[0]);
					}
				}
			}
			
			
		}
	
		return newResources;
	}
	
}
