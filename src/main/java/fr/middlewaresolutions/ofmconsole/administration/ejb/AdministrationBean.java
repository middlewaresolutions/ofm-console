/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.administration.ejb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.administration.model.Parameter;

/**
 * Bean for administration
 * @author Emmanuel
 *
 */
@Stateless(name="AdministrationBean", mappedName="AdministrationBean")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class AdministrationBean implements Administration {

	@PersistenceContext(unitName="administration")
	EntityManager em;
	
	private Logger LOG = Logger.getLogger(this.getClass().getName());
	
	/** Format de stockage */
	private SimpleDateFormat fdm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	/**
	 * 
	 */
	public AdministrationBean() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Calendar getConfigParameterDate(String name) {
		String value = getConfigParameterValue(name);
		Calendar cal = Calendar.getInstance();
		
		// Create Calendar if value exist
		if (value != null) {
			try {
				cal.setTime(fdm.parse(value));
			} catch (ParseException e) {
				LOG.severe("Date for "+name+" not parsable: "+e.getMessage());
			}
			return cal;
		}
		
		return null;
	}

	@Override
	public String getConfigParameterValue(String name) {
		Parameter p = em.find(Parameter.class, name);
		if (p != null)
			return p.getValue();
		else
			return null;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void storeConfigParameter(String name, Calendar date) {
		storeConfigParameter(name, fdm.format(date.getTime()));
	}

	/**
	 * Store Parameter
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void storeConfigParameter(String name, String value) {
		Parameter p = em.find(Parameter.class, name);
		
		if (p != null) {
			p.setValue(value);
		} else {
			p = new Parameter(name, value);
		}
		
		em.merge(p);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void storeDomain(Domain domain) {
		em.merge(domain);
	}

	@Override
	public List<Domain> listDomainsToScan() {
		return em.createQuery("select d from Domain d").getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String getConfigParameterValue(String name, String defaut) {
		String value = getConfigParameterValue(name);
		if (value == null) {
			storeConfigParameter(name, defaut);
		}
		
		return getConfigParameterValue(name);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Calendar getConfigParameterValue(String name, Calendar defaut) {
		Calendar value = getConfigParameterDate(name);
		if (value == null) {
			storeConfigParameter(name, defaut);
		}
		
		return getConfigParameterDate(name);
	}
}
