package fr.middlewaresolutions.ofmconsole.administration.api;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Local;

import fr.middlewaresolutions.ofmconsole.administration.model.Domain;

@Local
public interface Administration {

	/**
	 * Get value of a config parameter
	 * @param name
	 * @return
	 */
	String getConfigParameterValue(String name);

	/**
	 * Store name and value of a parameter
	 * @param name
	 * @param value
	 */
	void storeConfigParameter(String name, String value);

	/**
	 * Store a date as parameter
	 * @param name
	 * @param date
	 */
	void storeConfigParameter(String name, Calendar date);

	/**
	 * Retreive value of type Calendar
	 * @param name
	 * @return
	 */
	Calendar getConfigParameterDate(String name);

	/**
	 * Store domain to administration
	 * @param domain
	 */
	void storeDomain(Domain domain);

	/**
	 * List all domain to scan
	 * @return
	 */
	List<Domain> listDomainsToScan();

	/**
	 * Retreive a conf value, but indicate default value if not exist
	 * @param name of parameter
	 * @param defaut Default value
	 * @return
	 */
	String getConfigParameterValue(String name, String defaut);

	/**
	 * Retreive a conf date, but indicate default value if not exist
	 * @param name of parameter
	 * @param defaut Default value
	 * @return
	 */
	Calendar getConfigParameterValue(String name, Calendar defaut);
}