/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.administration.model;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.nosql.annotations.DataFormatType;
import org.eclipse.persistence.nosql.annotations.NoSql;

/**
 * @author Emmanuel
 *
 */
@Entity
@Cacheable
@Cache(size=100, expiry=20000)
public class Parameter {

	@Id
	private String name;
	
	private String value;
	
	/**
	 * 
	 */
	public Parameter() {
		// TODO Auto-generated constructor stub
	}

	public Parameter(String name, String value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
