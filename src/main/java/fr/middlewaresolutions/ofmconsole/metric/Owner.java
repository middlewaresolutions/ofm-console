/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.Index;
import org.eclipse.persistence.annotations.Indexes;
import org.eclipse.persistence.nosql.annotations.DataFormatType;
import org.eclipse.persistence.nosql.annotations.NoSql;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 * Owner of data
 * @author Emmanuel
 *
 */
@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@Cacheable
//@Indexes(value={
//		@Index(unique=true, columnNames="domain, server")
//})
public class Owner {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String domain;
	private String server;
	
	@XmlInverseReference(mappedBy="owner")
	@OneToMany(mappedBy="owner",fetch=FetchType.LAZY)
	List<Metric> metrics = new ArrayList<Metric>();
	
	@XmlInverseReference(mappedBy="owner")
	@OneToMany(mappedBy="owner",fetch=FetchType.LAZY)
	List<MetricPer5Min> metricsPer5Min = new ArrayList<MetricPer5Min>();
	
	@XmlInverseReference(mappedBy="owner")
	@OneToMany(mappedBy="owner",fetch=FetchType.LAZY)
	List<MetricPer15Min> metricsPer15Min = new ArrayList<MetricPer15Min>();
	
	@XmlInverseReference(mappedBy="owner")
	@OneToMany(mappedBy="owner",fetch=FetchType.LAZY)
	List<MetricPerHour> metricsPerHour = new ArrayList<MetricPerHour>();
	
	@XmlInverseReference(mappedBy="owner")
	@OneToMany(mappedBy="owner",fetch=FetchType.LAZY)
	List<MetricPerDay> metricsPerDay = new ArrayList<MetricPerDay>();
	
	/**
	 * 
	 */
	public Owner() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}

	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * @return the server
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/**
	 * @return the metrics
	 */
	public List<Metric> getMetrics() {
		return metrics;
	}

	/**
	 * @param metrics the metrics to set
	 */
	public void setMetrics(List<Metric> metrics) {
		this.metrics = metrics;
	}

	/**
	 * @return the metricsPer5Min
	 */
	public List<MetricPer5Min> getMetricsPer5Min() {
		return metricsPer5Min;
	}

	/**
	 * @param metricsPer5Min the metricsPer5Min to set
	 */
	public void setMetricsPer5Min(List<MetricPer5Min> metricsPer5Min) {
		this.metricsPer5Min = metricsPer5Min;
	}

	/**
	 * @return the metricsPer15Min
	 */
	public List<MetricPer15Min> getMetricsPer15Min() {
		return metricsPer15Min;
	}

	/**
	 * @param metricsPer15Min the metricsPer15Min to set
	 */
	public void setMetricsPer15Min(List<MetricPer15Min> metricsPer15Min) {
		this.metricsPer15Min = metricsPer15Min;
	}

	/**
	 * @return the metricsPerHour
	 */
	public List<MetricPerHour> getMetricsPerHour() {
		return metricsPerHour;
	}

	/**
	 * @param metricsPerHour the metricsPerHour to set
	 */
	public void setMetricsPerHour(List<MetricPerHour> metricsPerHour) {
		this.metricsPerHour = metricsPerHour;
	}

	/**
	 * @return the metricsPerDay
	 */
	public List<MetricPerDay> getMetricsPerDay() {
		return metricsPerDay;
	}

	/**
	 * @param metricsPerDay the metricsPerDay to set
	 */
	public void setMetricsPerDay(List<MetricPerDay> metricsPerDay) {
		this.metricsPerDay = metricsPerDay;
	}

}
