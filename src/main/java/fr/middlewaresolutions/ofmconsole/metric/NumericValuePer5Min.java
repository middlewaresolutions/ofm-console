/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="NumericValuePer5Min")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class NumericValuePer5Min extends AbstractStatisticValue {
	
	/**
	 * 
	 */
	public NumericValuePer5Min() {
		super();
	}

}
