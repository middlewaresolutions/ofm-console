/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKey;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="MetricPer15Min")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class MetricPer15Min extends AbstractMetric {

	public static final String GetNamesInInterval = "MetricPer15Min.GetNamesInInterval";
	public static final String GetNumericAttributeNamesInInterval = "MetricPer15Min.GetNumericAttributeNamesInInterval";
	public static final String GetNumericAttributesByNameInInterval = "MetricPer15Min.GetNumericAttributesByNameInInterval";
	
	@OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinTable(name="MetricPer15Min_NumericValuePer15Min",
    	joinColumns=@JoinColumn(name="MetricPer15Min_id"),
    	inverseJoinColumns=@JoinColumn(name="NumericValuePer15Min_id"))
	@MapKeyJoinColumn(name="name")
	@MapKey(name="name")
	private Map<String, NumericValuePer15Min> numericAttributes = new HashMap<String, NumericValuePer15Min>();
	
	/**
	 * 
	 */
	public MetricPer15Min() {
		super();
	}

	/**
	 * @param time
	 */
	public MetricPer15Min(Calendar time) {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the day
	 */
	public Calendar getDay() {
		return time;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(Calendar day) {
		this.time = day;
	}

	/**
	 * @return the numericAttributes
	 */
	public Collection<NumericValuePer15Min> getNumericAttributes() {
		return numericAttributes.values();
	}

	/**
	 * @param numericAttributes the numericAttributes to set
	 */
	public void addNumericAttribute(String key, NumericValuePer15Min numericAttribute) {
		this.numericAttributes.put(key, numericAttribute);
		
		numericAttribute.setTime(getDay());
	}

	@Override
	public AbstractValue getValue(String key) {
		AbstractValue value = numericAttributes.get(key);
		if (value == null)
			value = super.getValue(key);
		
		return value;
	}

}
