/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKey;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="MetricPerHour")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class MetricPerHour extends AbstractMetric {

	public static final String GetNamesInInterval = "MetricPerHour.GetNamesInInterval";
	public static final String GetNumericAttributeNamesInInterval = "MetricPerHour.GetNumericAttributeNamesInInterval";
	public static final String GetNumericAttributesByNameInInterval = "MetricPerHour.GetNumericAttributesByNameInInterval";
	
	@OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinTable(name="MetricPerHour_NumericValuePerHour",
    	joinColumns=@JoinColumn(name="MetricPerHour_id"),
    	inverseJoinColumns=@JoinColumn(name="NumericValuePerHour_id"))
	@MapKeyJoinColumn(name="name")
	@MapKey(name="name")
	private Map<String, NumericValuePerHour> numericAttributes = new HashMap<String, NumericValuePerHour>();
	
	/**
	 * 
	 */
	public MetricPerHour() {
		super();
	}

	/**
	 * @param time
	 */
	public MetricPerHour(Calendar time) {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the hour
	 */
	public Calendar getDay() {
		return time;
	}

	/**
	 * @param hour the hour to set
	 */
	public void setDay(Calendar hour) {
		this.time = hour;
	}

	/**
	 * @return the numericAttributes
	 */
	public Collection<NumericValuePerHour> getNumericAttributes() {
		return numericAttributes.values();
	}

	/**
	 * @param numericAttributes the numericAttributes to set
	 */
	public void addNumericAttribute(String key, NumericValuePerHour numericAttribute) {
		this.numericAttributes.put(key, numericAttribute);
		
		numericAttribute.setTime(getDay());
	}

	@Override
	public AbstractValue getValue(String key) {
		AbstractValue value = numericAttributes.get(key);
		if (value == null)
			value = super.getValue(key);
		
		return value;
	}


}
