/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Index;
import org.eclipse.persistence.annotations.Indexes;

/**
 * Unique representation of a attribute for a Metric
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="MetricAttribute")
@NamedQueries({
	@NamedQuery(name="MetricAttribute.FindByJmxFullAndAttributeName", query="select distinct m from MetricAttribute m where m.jmxFull=:jmxFull and m.attributeName = :attributeName"),
	@NamedQuery(name="MetricAttribute.FindByJmxFull", query="select distinct m from MetricAttribute m where m.jmxFull = :jmxFull and m.isNumeric = :numeric"),
	@NamedQuery(name="MetricAttribute.FindAttributeNameByJmxType", query="select distinct m.attributeName from MetricAttribute m where m.jmxType = :jmxType"),
	@NamedQuery(name="MetricAttribute.FindByJmxTypeAndNumeric", query="select distinct m from MetricAttribute m where m.jmxType = :jmxType and m.isNumeric = :numeric")
})
@Cacheable
//@Indexes({
//	@Index(name="MetricAttribute.jmxType", columnNames="jmx_type"),
//	@Index(name="MetricAttribute.jmxFullName", columnNames="jmx_full,name ")
//})
public class MetricAttribute implements Serializable {

	public final static String FindByJmxFullAndAttributeName = "MetricAttribute.FindByJmxFullAndAttributeName";
	public final static String FindByJmxFull = "MetricAttribute.FindByJmxFull";
	public final static String FindAttributeNameByJmxType = "MetricAttribute.FindAttributeNameByJmxType";
	public final static String FindByJmxTypeAndNumeric = "MetricAttribute.FindByJmxTypeAndNumeric";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="jmx_full", length=512)
	private String jmxFull;
	
	// JMX Information
	@Column(name="jmx_location")
	private String jmxLocation;
	
	@Column(name="jmx_name")
	private String jmxName;
	
	@Column(name="jmx_serverRuntime")
	private String jmxServerRuntime;
	
	@Column(name="jmx_type")
	private String jmxType;
	
	@Column(name="attribute_name", length=256)
	private String attributeName;
	
	@Column(name="description", length=2048)
	private String description;
	
	@Column(name="isNumeric")
	private Boolean isNumeric = false;
	
	/**
	 * 
	 */
	public MetricAttribute() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the jmxLocation
	 */
	public String getJmxLocation() {
		return jmxLocation;
	}

	/**
	 * @param jmxLocation the jmxLocation to set
	 */
	public void setJmxLocation(String jmxLocation) {
		this.jmxLocation = jmxLocation;
	}

	/**
	 * @return the jmxName
	 */
	public String getJmxName() {
		return jmxName;
	}

	/**
	 * @param jmxName the jmxName to set
	 */
	public void setJmxName(String jmxName) {
		this.jmxName = jmxName;
	}

	/**
	 * @return the jmxServerRuntime
	 */
	public String getJmxServerRuntime() {
		return jmxServerRuntime;
	}

	/**
	 * @param jmxServerRuntime the jmxServerRuntime to set
	 */
	public void setJmxServerRuntime(String jmxServerRuntime) {
		this.jmxServerRuntime = jmxServerRuntime;
	}

	/**
	 * @return the attributeName
	 */
	public String getAttributeName() {
		return attributeName;
	}

	/**
	 * @param attributeName the attributeName to set
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the jmxFull
	 */
	public String getJmxFull() {
		return jmxFull;
	}

	/**
	 * @param jmxFull the jmxFull to set
	 */
	public void setJmxFull(String jmxFull) {
		this.jmxFull = jmxFull;
	}

	/**
	 * @return the jmxType
	 */
	public String getJmxType() {
		return jmxType;
	}

	/**
	 * @param jmxType the jmxType to set
	 */
	public void setJmxType(String jmxType) {
		this.jmxType = jmxType;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the numeric
	 */
	public boolean isNumeric() {
		return isNumeric;
	}

	/**
	 * @param numeric the numeric to set
	 */
	public void setNumeric(boolean numeric) {
		this.isNumeric = numeric;
	}

}
