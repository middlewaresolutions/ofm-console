/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.Index;
import org.eclipse.persistence.annotations.Indexes;

/**
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="NumericValue")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@NamedQueries({
	@NamedQuery(name="NumericValue.FindByMetricTypeInInterval", 
			query="select nv from Metric m join m.numericAttributes nv where m.jmx_type=:jmxType and m.time between :start and :end order by m.time asc"),
	@NamedQuery(name="NumericValue.FindByMetricTypeAndNameInInterval", 
			query="select nv from Metric m join m.numericAttributes nv where m.jmx_type=:jmxType and m.time between :start and :end and nv.name = :attributeName order by m.time asc"),
	@NamedQuery(name="NumericValue.FindByMetricJmxNameAndNameInInterval", 
		query="select nv from Metric m join m.numericAttributes nv where m.jmx_name=:jmxName and m.time between :start and :end and nv.name = :attributeName order by m.time asc")
})
//@Indexes({
//	@Index(name="NumericValue.name", columnNames="name"),
//	@Index(name="NumericValue.time", columnNames="time")
//})
public class NumericValue extends AbstractNumericValue {
	
	public static String FindByMetricJmxTypeAndNameInInterval = "NumericValue.FindByMetricTypeAndNameInInterval";
	public static String FindByMetricJmxNameAndNameInInterval = "NumericValue.FindByMetricJmxNameAndNameInInterval";
	public static String FindByMetricTypeInInterval = "NumericValue.FindByMetricTypeInInterval";

	/**
	 * 
	 */
	public NumericValue() {
		super();
	}

	public NumericValue(String name, Double value) {
		super();
		setName(name);
		setValue(value);
	}

}
