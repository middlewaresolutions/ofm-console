/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKey;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="MetricPerDay")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class MetricPerDay extends AbstractMetric {

	public static final String GetNamesInInterval = "MetricPerDay.GetNamesInInterval";
	public static final String GetNumericAttributeNamesInInterval = "MetricPerDay.GetNumericAttributeNamesInInterval";
	public static final String GetNumericAttributesByNameInInterval = "MetricPerDay.GetNumericAttributesByNameInInterval";
	
	@OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinTable(name="MetricPerDay_NumericValuePerDay",
    	joinColumns=@JoinColumn(name="MetricPerDay_id"),
    	inverseJoinColumns=@JoinColumn(name="NumericValuePerDay_id"))
	@MapKeyJoinColumn(name="name")
	@MapKey(name="name")
	private Map<String, NumericValuePerDay> numericAttributes = new HashMap<String, NumericValuePerDay>();
	
	/**
	 * 
	 */
	public MetricPerDay() {
		super();
	}

	/**
	 * @param time
	 */
	public MetricPerDay(Calendar time) {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the day
	 */
	public Calendar getDay() {
		return time;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(Calendar day) {
		this.time = day;
	}

	/**
	 * @return the numericAttributes
	 */
	public Collection<NumericValuePerDay> getNumericAttributes() {
		return numericAttributes.values();
	}

	/**
	 * @param numericAttributes the numericAttributes to set
	 */
	public void addNumericAttribute(String key, NumericValuePerDay numericAttribute) {
		this.numericAttributes.put(key, numericAttribute);
		
		numericAttribute.setTime(getDay());
	}

	@Override
	public AbstractValue getValue(String key) {
		AbstractValue value = numericAttributes.get(key);
		if (value == null)
			value = super.getValue(key);
		
		return value;
	}

}
