/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Emmanuel
 *
 */
@Entity
@Table(name="TextValue")
public class TextValue extends AbstractValue {
	
	@Column(length=AbstractValue.MaxLength)
	private String value;
	
	/**
	 * 
	 */
	public TextValue() {
		super();
	}

	public TextValue(String name, String value) {
		super();
		setName(name);
		setValue(value);
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
