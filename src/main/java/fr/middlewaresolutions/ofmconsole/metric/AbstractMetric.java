/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.Index;
import org.eclipse.persistence.annotations.Indexes;
import org.eclipse.persistence.nosql.annotations.DataFormatType;
import org.eclipse.persistence.nosql.annotations.NoSql;

/**
 * Abstract metric
 * @author Emmanuel
 *
 */
@MappedSuperclass
@Access(AccessType.FIELD)
@Cacheable
@Cache(size=2000, expiry=30000)
//@Indexes({
//	@Index(name="AbstractMetric.name", columnNames="name"),
//	@Index(name="AbstractMetric.jmx_type", columnNames="jmx_type"),
//	@Index(name="AbstractMetric.time", columnNames="time")
//})
public abstract class AbstractMetric implements Serializable {

	public final static int ValueMaxLength = 2048;
	public final static int KeyMaxLength = 512;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
        
	@Column(name="name", length=KeyMaxLength, nullable=false)
	private String name;
	
	@OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true)
	@JoinTable(name="Metric_TextValue",
            joinColumns=@JoinColumn(name="metric_id"),
            inverseJoinColumns=@JoinColumn(name="textvalue_id"))
	@MapKeyJoinColumn(name="name")
	@MapKey(name="name")
	private Map<String, TextValue> textAttributes;
	
	@ManyToOne(cascade={CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.DETACH})
	private Owner owner;
	
	// Extract from JMX
	private String jmx_location;
	private String jmx_name;
	private String jmx_serverRuntime;
	
	private String jmx_type;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	protected Calendar time;
	
	public AbstractMetric() {
		textAttributes = new HashMap<String, TextValue>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the textAttributes
	 */
	public Collection<TextValue> getTextAttributes() {
		return textAttributes.values();
	}

	/**
	 * @param textAttributes the textAttributes to set
	 */
	public void addTextAttribute(String key, TextValue textAttribute) {
		this.textAttributes.put(key, textAttribute);
		
		textAttribute.setTime(time);
	}

	/**
	 * Return value of a attribute
	 * @param key
	 * @return
	 */
	public AbstractValue getValue(String key) {
		return textAttributes.get(key);
	}

	/**
	 * @return the owner
	 */
	public Owner getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
	/**
	 * @return the jmx_location
	 */
	public String getJmx_location() {
		return jmx_location;
	}

	/**
	 * @param jmx_location the jmx_location to set
	 */
	public void setJmx_location(String jmx_location) {
		this.jmx_location = jmx_location;
	}

	/**
	 * @return the jmx_name
	 */
	public String getJmx_name() {
		return jmx_name;
	}

	/**
	 * @param jmx_name the jmx_name to set
	 */
	public void setJmx_name(String jmx_name) {
		this.jmx_name = jmx_name;
	}

	/**
	 * @return the jmx_serverRuntime
	 */
	public String getJmx_serverRuntime() {
		return jmx_serverRuntime;
	}

	/**
	 * @param jmx_serverRuntime the jmx_serverRuntime to set
	 */
	public void setJmx_serverRuntime(String jmx_serverRuntime) {
		this.jmx_serverRuntime = jmx_serverRuntime;
	}

	/**
	 * @return the jmx_type
	 */
	public String getJmx_type() {
		return jmx_type;
	}

	/**
	 * @param jmx_type the jmx_type to set
	 */
	public void setJmx_type(String jmx_type) {
		this.jmx_type = jmx_type;
	}
	
	/**
	 * Get value with the key
	 * @param key
	 * @return
	 */
	public Object getAttributeValue(String key) {
		if (textAttributes.containsKey(key))
			return textAttributes.get(key).getValue();
		else
			return null;
	}
}
