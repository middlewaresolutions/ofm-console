/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKey;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.annotations.Index;
import org.eclipse.persistence.annotations.Indexes;

/**
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="Metric")
@NamedQueries(value={
		@NamedQuery(name="Metric.FindWithInterval", query="select m from Metric m where m.time >= :begin and m.time <= :end"),
		@NamedQuery(name="Metric.MaxOfName", query="select max(m.time) from Metric m where m.name like :name"),
		@NamedQuery(name="Metric.MaxOfType", query="select max(m.time) from Metric m where m.jmx_type like :type"),
		@NamedQuery(name="Metric.SelectByNameAndTime", query="select m from Metric m where m.name like :name and m.time=:time"),
		@NamedQuery(name="Metric.SelectByTypeAndTime", query="select m from Metric m where m.jmx_type like :type and m.time=:time"),
		@NamedQuery(name="Metric.SelectByNameAndInterval", query="select m from Metric m where m.name like :name and m.time >= :begin and m.time <= :end"),
		@NamedQuery(name="Metric.GetNamesInInterval", query="select distinct m.name from Metric m where m.time >= :begin and m.time <= :end"),
		@NamedQuery(name="Metric.GetNumericAttributeNamesInInterval", query="select distinct n.name from Metric m join m.numericAttributes n where m.name = :name and m.time >= :begin and m.time <= :end"),
		@NamedQuery(name="Metric.GetNumericAttributesByNameInInterval", query="select n from Metric m join m.numericAttributes n where m.name = :metricName and n.name = :attributeName and m.time >= :begin and m.time <= :end"),
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Metric extends AbstractMetric {

	public final static String FindWithInterval = "Metric.FindWithInterval";
	public static final String MaxOfName = "Metric.MaxOfName";
	public static final String SelectByNameAndTime = "Metric.SelectByNameAndTime";
	public static final String SelectByNameAndInterval = "Metric.SelectByNameAndInterval";
	
	public static final String GetNamesInInterval = "Metric.GetNamesInInterval";
	public static final String GetNumericAttributeNamesInInterval = "Metric.GetNumericAttributeNamesInInterval";
	public static final String GetNumericAttributesByNameInInterval = "Metric.GetNumericAttributesByNameInInterval";
	
	public static final String MaxOfType = "Metric.MaxOfType";
	public static final String SelectByTypeAndTime = "Metric.SelectByTypeAndTime";

	@OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinTable(name="Metric_NumericValue",
    	joinColumns=@JoinColumn(name="Metric_id"),
    	inverseJoinColumns=@JoinColumn(name="NumericValue_id"))
	@MapKeyJoinColumn(name="name")
	@MapKey(name="name")
	private Map<String, NumericValue> numericAttributes;
	
	
	
	public Metric() {
		this(Calendar.getInstance());
	}
	
	/**
	 * 
	 */
	public Metric(Calendar time) {
		super();
		numericAttributes = new HashMap<String, NumericValue>();
		this.time = time;
	}

	/**
	 * @return the time
	 */
	public Calendar getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Calendar time) {
		this.time = time;
	}

	/**
	 * @return the numericAttributes
	 */
	public Collection<NumericValue> getNumericAttributes() {
		return numericAttributes.values();
	}

	/**
	 * @param numericAttributes the numericAttributes to set
	 */
	public void addNumericAttribute(String key, NumericValue attribute) {
		this.numericAttributes.put(key, attribute);
		
		attribute.setTime(getTime());
	}

	@Override
	public AbstractValue getValue(String key) {
		AbstractValue value = numericAttributes.get(key);
		if (value == null)
			value = super.getValue(key);
		
		return value;
	}

	/**
	 * Return the numeric value of the key
	 */
	@Override
	public Object getAttributeValue(String key) {
		if (numericAttributes.containsKey(key))
			return numericAttributes.get(key).getValue();
		else
			return super.getAttributeValue(key);
	}
	
}
