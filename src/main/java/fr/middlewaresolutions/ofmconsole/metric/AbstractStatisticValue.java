package fr.middlewaresolutions.ofmconsole.metric;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Access(AccessType.FIELD)
public class AbstractStatisticValue extends AbstractValue {
	
	private Long quantity;
	
	@Column(name="average")
	private Double avg;
	
	@Column(name="minimum")
	private Double min;
	
	@Column(name="maximum")
	private Double max;
	
	@Column(name="standard_deviation")
	private Double standardDeviation;
	
	private Double percentil90;
	private Double percentile95;
	private Double percentile99;
	
	private Double variation;
	
	
	/**
	 * @return the quantity
	 */
	public Long getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the avg
	 */
	public Double getAvg() {
		return avg;
	}
	/**
	 * @param avg the avg to set
	 */
	public void setAvg(Double avg) {
		this.avg = avg;
	}
	/**
	 * @return the min
	 */
	public Double getMin() {
		return min;
	}
	/**
	 * @param min the min to set
	 */
	public void setMin(Double min) {
		this.min = min;
	}
	/**
	 * @return the max
	 */
	public Double getMax() {
		return max;
	}
	/**
	 * @param max the max to set
	 */
	public void setMax(Double max) {
		this.max = max;
	}
	/**
	 * @return the ecartType
	 */
	public Double getEcartType() {
		return standardDeviation;
	}
	/**
	 * @param ecartType the ecartType to set
	 */
	public void setEcartType(Double ecartType) {
		this.standardDeviation = ecartType;
	}
	/**
	 * @return the percentil90
	 */
	public Double getPercentil90() {
		return percentil90;
	}
	/**
	 * @param percentil90 the percentil90 to set
	 */
	public void setPercentil90(Double percentil90) {
		this.percentil90 = percentil90;
	}
	/**
	 * @return the percentile95
	 */
	public Double getPercentile95() {
		return percentile95;
	}
	/**
	 * @param percentile95 the percentile95 to set
	 */
	public void setPercentile95(Double percentile95) {
		this.percentile95 = percentile95;
	}
	/**
	 * @return the percentile99
	 */
	public Double getPercentile99() {
		return percentile99;
	}
	/**
	 * @param percentile99 the percentile99 to set
	 */
	public void setPercentile99(Double percentile99) {
		this.percentile99 = percentile99;
	}
	/**
	 * @return the variation
	 */
	public Double getVariation() {
		return variation;
	}
	/**
	 * @param variation the variation to set
	 */
	public void setVariation(Double variation) {
		this.variation = variation;
	}
	
	
}
