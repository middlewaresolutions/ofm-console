/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKey;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="MetricPer5Min")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class MetricPer5Min extends AbstractMetric {

	public static final String GetNamesInInterval = "MetricPer5Min.GetNamesInInterval";
	public static final String GetNumericAttributeNamesInInterval = "MetricPer5Min.GetNumericAttributeNamesInInterval";
	public static final String GetNumericAttributesByNameInInterval = "MetricPer5Min.GetNumericAttributesByNameInInterval";

	
	
	@OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true, fetch=FetchType.LAZY)
	@JoinTable(name="MetricPer5Min_NumericValuePer5Min",
    	joinColumns=@JoinColumn(name="MetricPer5Min_id"),
    	inverseJoinColumns=@JoinColumn(name="NumericValuePer5Min_id"))
	@MapKeyJoinColumn(name="name")
	@MapKey(name="name")
	private Map<String, NumericValuePer5Min> numericAttributes = new HashMap<String, NumericValuePer5Min>();
	
	/**
	 * 
	 */
	public MetricPer5Min() {
		super();
	}

	/**
	 * @param time
	 */
	public MetricPer5Min(Calendar time) {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the day
	 */
	public Calendar getDay() {
		return time;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(Calendar day) {
		this.time = day;
	}

	/**
	 * @return the numericAttributes
	 */
	public Collection<NumericValuePer5Min> getNumericAttributes() {
		return numericAttributes.values();
	}

	/**
	 * @param numericAttributes the numericAttributes to set
	 */
	public void addNumericAttribute(String key, NumericValuePer5Min numericAttribute) {
		this.numericAttributes.put(key, numericAttribute);
		numericAttribute.setTime(getDay());
	}

	@Override
	public AbstractValue getValue(String key) {
		AbstractValue value = numericAttributes.get(key);
		if (value == null)
			value = super.getValue(key);
		
		return value;
	}

	


}
