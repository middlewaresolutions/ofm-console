/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.metric;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@Entity
@Access(AccessType.FIELD)
@Table(name="NumericValuePerDay")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class NumericValuePerDay extends AbstractStatisticValue {
	
	/**
	 * 
	 */
	public NumericValuePerDay() {
		super();
	}

}
