/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerChannel extends Base {

	private String serverName;
	
	/**
	 * 
	 */
	public ServerChannel() {
		super();
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

}
