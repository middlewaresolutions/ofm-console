/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

/**
 * @author Emmanuel
 *
 */
public class DataSource extends Base {

	private String STATUS = "State";
	
	private String serverName;
	
	/**
	 * 
	 */
	public DataSource() {
		super.STATUS = this.STATUS;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

}
