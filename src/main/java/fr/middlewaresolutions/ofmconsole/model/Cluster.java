/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Cluster representation
 * @author Emmanuel
 *
 */
public class Cluster extends Base {

	List<Server> servers = new ArrayList<Server>();
	
	public Cluster() {
		super();
	}
	/**
	 * @return the servers
	 */
	public List<Server> getServers() {
		return servers;
	}
	/**
	 * @param servers the servers to set
	 */
	public void setServers(List<Server> servers) {
		this.servers = servers;
	}

}
