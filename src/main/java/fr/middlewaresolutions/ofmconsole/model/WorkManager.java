/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class WorkManager extends Base {

	private ThreadConstraint max;
	private ThreadConstraint min;
	
	/**
	 * @return the max
	 */
	public ThreadConstraint getMax() {
		return max;
	}

	/**
	 * @param max the max to set
	 */
	public void setMax(ThreadConstraint max) {
		this.max = max;
	}

	/**
	 * @return the min
	 */
	public ThreadConstraint getMin() {
		return min;
	}

	/**
	 * @param min the min to set
	 */
	public void setMin(ThreadConstraint min) {
		this.min = min;
	}

	/**
	 * 
	 */
	public WorkManager() {
		super();
	}

}
