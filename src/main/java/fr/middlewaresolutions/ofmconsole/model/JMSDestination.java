/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

/**
 * @author Emmanuel
 *
 */
public class JMSDestination extends Base {

	private String STATUS = "State";
	private String serverName;
	
	/**
	 * 
	 */
	public JMSDestination() {
		super.STATUS = this.STATUS;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * JMS Module name
	 * @return
	 */
	public String getModuleName() {
		return ((String)getAttributeValue("Name")).split("!")[0];
	}
	
	/**
	 * Short name of destination
	 * @return
	 */
	public String getShortName() {
		return ((String)getAttributeValue("Name")).split("!")[1];
	}
}
