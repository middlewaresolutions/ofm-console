/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

/**
 * Metric from DMS
 * @author Emmanuel
 *
 */
public class DMSMetric extends Base {

	private String serverName;
	
	/**
	 * 
	 */
	public DMSMetric() {
		super();
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

}
