package fr.middlewaresolutions.ofmconsole.model;

import java.util.HashMap;

public class OSBResource extends Base {

	private String serverName;
	
	private HashMap<String, Object> statistics = new HashMap<String, Object>();
	
	public OSBResource() {
		super();
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public HashMap<String, Object> getStatistics() {
		return statistics;
	}

	public void setStatistics(HashMap<String, Object> statistics) {
		this.statistics = statistics;
	}

}
