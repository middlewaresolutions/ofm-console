/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Application extends Base {

	private List<ComponentRuntime> components = new ArrayList<ComponentRuntime>();
	
	private String serverName;
	
	/**
	 * 
	 */
	public Application() {
		super();
	}

	public List<ComponentRuntime> getComponents() {
		return components;
	}

	public void addComponents(ComponentRuntime component) {
		this.components.add(component);
		component.setParent(this);
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

}
