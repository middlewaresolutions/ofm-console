package fr.middlewaresolutions.ofmconsole.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class Base implements Serializable {

	protected String NAME = "Name";
	protected String STATUS = "Status";

	private String jmxObjectName;
	
	private Calendar time;
	
	/** Attributes of elements*/
	private HashMap<String, Object> attributes = new HashMap<String, Object>();
	
	/** Attrbute by target (server, cluster, etc) */
	private ArrayList<Base> targets = new ArrayList<Base>();
	
	private Base parent;
	
	private String id = UUID.randomUUID().toString();
	
	/**
	 * Default constructor with Name as attribute PK
	 */
	protected Base() {
		setTime(Calendar.getInstance());
	}
	
	/**
	 * @return the jmxObjectName
	 */
	public String getJmxObjectName() {
		return jmxObjectName;
	}

	/**
	 * @param jmxObjectName the jmxObjectName to set
	 */
	public void setJmxObjectName(String jmxObjectName) {
		this.jmxObjectName = jmxObjectName;
		
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return (String) getAttributeValue(NAME);
	}
	
	public String getStatus() {
		return (String) getAttributeValue(STATUS);
	}
	
	/**
	 * Add an attribute
	 * @param name
	 * @param value
	 */
	public void addAttribute(String name, Object value) {
		attributes.put(name, value);
	}

	/**
	 * Get attribute value
	 * @param name
	 * @return
	 */
	public Object getAttributeValue(String name) {
		return attributes.get(name);
	}
	
	/**
	 * Extract status from weblogic
	 * @return
	 */
	public String getHealth() {
		Object health = getAttributeValue("HealthState");
		
		if (health != null) {
			String[] wlsStatus = getAttributeValue("HealthState").toString().split(",");
			for(String part: wlsStatus){
				if (part.startsWith("State:"))
					return part.split(":")[1];
			}
		}
		
		return "Unknown";
	}

	public List<Base> getTargets() {
		return targets;
	}

	public void addTarget(Base target) {
		targets.add(target);
		target.setParent(this);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Base clone() throws CloneNotSupportedException {
		Base clone = null;
		try {
			clone = (Base) this.getClass().newInstance();
			
			clone.setJmxObjectName(this.jmxObjectName);
			clone.targets = this.targets;
			clone.attributes = this.attributes;
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return clone;
	}

	public Base getParent() {
		return parent;
	}

	public void setParent(Base parent) {
		this.parent = parent;
	}
	
	/**
	 * Keys of attributes
	 * @return
	 */
	public Set<String> getAttributeKeys() {
		return attributes.keySet();
	}

	public Calendar getTime() {
		return time;
	}

	public void setTime(Calendar time) {
		this.time = time;
	}
	
}
