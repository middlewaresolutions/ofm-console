/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Emmanuel
 *
 */
public class SCAComposite extends Base {

	private String STATUS = "State";
	
	private List<SCAComponent> components = new ArrayList<SCAComponent>();
	private List<SCABinding> references = new ArrayList<SCABinding>();
	private List<SCABinding> services = new ArrayList<SCABinding>();
	
	private String serverName;
	
	/**
	 * 
	 */
	public SCAComposite() {
		super();
		super.STATUS = this.STATUS;
	}

	/**
	 * @return the components
	 */
	public List<SCAComponent> getComponents() {
		return components;
	}

	/**
	 * @return the references
	 */
	public List<SCABinding> getReferences() {
		return references;
	}

	/**
	 * @return the services
	 */
	public List<SCABinding> getServices() {
		return services;
	}

	public void addComponent(SCAComponent component){
		components.add(component);
		component.setParent(this);
	}
	
	public void addReference(SCABinding ref){
		references.add(ref);
		ref.setParent(this);
	}
	
	public void addService(SCABinding service){
		services.add(service);
		service.setParent(this);
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
}
