/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Emmanuel
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ThreadConstraint extends Base {

	/**
	 * 
	 */
	public ThreadConstraint() {
		super();
	}

}
