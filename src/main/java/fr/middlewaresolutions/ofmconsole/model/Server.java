package fr.middlewaresolutions.ofmconsole.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Server extends Base {

	private Boolean admin;
	
	private String clusterName;
	private String STATUS = "StartupMode";
	private String CLUSTERNAME = "Cluster";
	private String ISADMIN = "AdminServer";
	
	private JVM jvm;
	
	public Server() {
		super();
		super.STATUS = this.STATUS;
	}
	
	public Boolean isAdmin() {
		return Boolean.valueOf((Boolean)getAttributeValue(ISADMIN));
	}
	
	public String getClusterName() {
		return (String) getAttributeValue(CLUSTERNAME);
	}
	
}
