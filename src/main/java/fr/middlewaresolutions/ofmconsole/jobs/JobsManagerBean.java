package fr.middlewaresolutions.ofmconsole.jobs;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.api.JobsManager;

/**
 * WebApp listener for Jobs
 * 
 * @author Emmanuel
 *
 */
@Singleton(name="JobsManagerBean", mappedName="JobsManagerBean")
public class JobsManagerBean implements JobsManager {

	/** Quatrz Job Scheduler*/
	private static Scheduler scheduler;
	private static String LOCK = "LOCK";
	
	protected Logger LOG = Logger.getLogger(this.getClass().getName());
	
	@EJB
	Administration admin;
	
	@Inject
    private CdiJobFactory cdiJobFactory;
	
	public JobsManagerBean() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void stop() {
		synchronized(LOCK) {
			try {
				scheduler.pauseAll();
			} catch (SchedulerException e) {
				LOG.warning("Pause all jobs impossible: "+e.getMessage());
			}
			
			// Stop all
			try {
				scheduler.shutdown(false);
			} catch (SchedulerException e) {
				LOG.warning("Shutdown all jobs impossible: "+e.getMessage());
			}
		}
	}
	
	private static String CronServer = "CronServer";
	private static String CronCluster = "CronCluster";
	private static String CronChannelRuntime = "CronChannelRuntime";
	private static String CronJVM = "CronJVM";
	private static String CronJMS = "CronJMS";
	private static String CronApplication = "CronApplication";
	private static String CronBPELEngine = "CronBPELEngine";
	private static String CronDataSource = "CronDataSource";
	private static String CronMessageProcessing = "CronMessageProcessing";
	private static String CronOSB = "CronOSB";
	private static String CronSCAComposite = "CronSCAComposite";
	private static String CronWorkManager = "CronWorkManager";
	private static String CronAggregate5Min = "CronAggregate5Min";
	private static String CronAggregate15Min = "CronAggregate15Min";
	private static String CronAggregateHour = "CronAggregateHour";
	private static String CronAggregateDay = "CronAggregateDay";
	private static String CronDeleteMetric = "CronDeleteMetric";
	private static String CronDeleteMetricPer5Min = "CronDeleteMetricPer5Min";
	private static String CronDeleteMetricPer15Min = "CronDeleteMetricPer15Min";
	private static String CronDeleteMetricPerHour = "CronDeleteMetricPerHour";
	
	/**
	 * Get CRON Value with a default value
	 * @param name
	 * @param defaut
	 */
	private String getCron(String name, String defaut) {
		return admin.getConfigParameterValue(name, defaut);
	}
	
	@Override
	public void start() {

		String cron15s = "0/15 * * * * ?";
		String cron30s = "0/30 * * * * ?";
		String cron1min = "0 * * * * ?";
		String cron2min = "0 0/2 * * * ?";
		String cron5min = "0 0/5 * * * ?";
		
		List<String[]> jobs = new ArrayList<>();
		jobs.add(new String[] {"jmx.Server", getCron(CronServer,cron1min)});
		jobs.add(new String[] {"jmx.Cluster", getCron(CronCluster,cron2min)});
		jobs.add(new String[] {"jmx.ChannelRuntime", getCron(CronChannelRuntime,cron1min)});
		jobs.add(new String[] {"jmx.JVM", getCron(CronJVM,cron30s)});
		jobs.add(new String[] {"jmx.JMS", getCron(CronJMS,cron30s)});
		jobs.add(new String[] {"jmx.Application", getCron(CronApplication,cron2min)});
		jobs.add(new String[] {"jmx.BPELEngine", getCron(CronBPELEngine,cron30s)});
		jobs.add(new String[] {"jmx.DataSource", getCron(CronDataSource,cron30s)});
		jobs.add(new String[] {"jmx.MessageProcessing", getCron(CronMessageProcessing,cron1min)});
		jobs.add(new String[] {"jmx.OSB", getCron(CronOSB,cron5min)});
		jobs.add(new String[] {"jmx.SCAComposite", getCron(CronSCAComposite,cron2min)});
		jobs.add(new String[] {"jmx.WorkManager", getCron(CronWorkManager,cron30s)});
		jobs.add(new String[] {"aggregate.Aggregate5Min", getCron(CronAggregate5Min,"0 1/5 * * * ?")});
		jobs.add(new String[] {"aggregate.Aggregate15Min", getCron(CronAggregate15Min,"0 1/15 * * * ?")});
		jobs.add(new String[] {"aggregate.AggregateHour", getCron(CronAggregateHour,"0 4 * * * ?")});
		jobs.add(new String[] {"aggregate.AggregateDay", getCron(CronAggregateDay,"0 0 1 * * ?")});
		
		jobs.add(new String[] {"delete.DeleteMetric", getCron(CronDeleteMetric,"0 0 0/2 * * ?")});
		jobs.add(new String[] {"delete.DeleteMetricPer5Min", getCron(CronDeleteMetricPer5Min,"0 0 0/2 * * ?")});
		jobs.add(new String[] {"delete.DeleteMetricPer15Min", getCron(CronDeleteMetricPer15Min,"0 0 0/2 * * ?")});
		jobs.add(new String[] {"delete.DeleteMetricPerHour", getCron(CronDeleteMetricPerHour,"0 0 1 * * ?")});
		
		synchronized(LOCK) {
			try {
				scheduler = new StdSchedulerFactory().getScheduler();
				
				// inject CDIJobFactory to use CDI
				scheduler.setJobFactory(cdiJobFactory);
				
				for(String[] conf: jobs) {
					Trigger trigger = TriggerBuilder
							.newTrigger()
							.withIdentity(conf[0], "global")
							.withSchedule(
								CronScheduleBuilder.cronSchedule(conf[1]))
							.build();
					
					// JMX Server
					JobKey jobKey = new JobKey(conf[0], "global");
					JobDetail job = JobBuilder
										.newJob((Class<? extends Job>) this.getClass().forName("fr.middlewaresolutions.ofmconsole.jobs."+conf[0]+"Job"))
										.withIdentity(jobKey).build();
					scheduler.scheduleJob(job, trigger);
				}
				
				scheduler.start();
				
			} catch (SchedulerException | ClassNotFoundException e) {
				LOG.severe("Error Quartz initialization: "+e.getMessage());
			}
		}
	}

}
