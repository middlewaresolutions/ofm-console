/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.middlewaresolutions.ofmconsole.jobs;

import java.util.logging.Logger;

import javax.ejb.EJB;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.api.AggregateMetrics;
import fr.middlewaresolutions.ofmconsole.api.CleanStorage;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;

/**
 * Abstract Job
 * @author Emmanuel
 */
@DisallowConcurrentExecution
public abstract class AbstractJob implements Job {

	protected Logger LOG = Logger.getLogger(this.getClass().getName());
	
	@EJB
	protected Administration admin;
	
	@EJB
	protected PersistMetrics persistMetrics;
	
	@EJB
	protected Infrastructure infra;
	
	@EJB
	protected CleanStorage clean;
	
	@EJB
	protected AggregateMetrics agg;
    
}
