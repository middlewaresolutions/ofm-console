/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.middlewaresolutions.ofmconsole.jobs.aggregate;

import java.io.IOException;
import java.util.Calendar;

import javax.naming.NamingException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.jobs.AbstractJob;

/**
 * Scan JMX Server information
 * @author Emmanuel
 */
public class AggregateDayJob extends AbstractJob {
	
	private static String ConfigLastScanPerDay = "ConfigLastScanPerDay";
	
	/**
	 * Return last scan store in DB
	 * @throws NamingException 
	 */
	private Calendar getLastScanPerDay() throws NamingException {		
		Calendar lastCalendar = admin.getConfigParameterDate(this.ConfigLastScanPerDay);
		
		if (lastCalendar == null)
			lastCalendar = Calendar.getInstance();
		
		return lastCalendar;
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LOG.info("Aggregate Per Day");

	    Calendar start = Calendar.getInstance();
	    Calendar end = null; 
	    
	    // aggregate last 5 min
	    try {
	    	persistMetrics.storePerformance("AggregateTimer-PerDay", start, null);
	    	
			end = agg.aggregatePerDay(getLastScanPerDay());

		} catch (Exception e) {
			LOG.warning("Error during aggregation per Day :"+e.getMessage());
		}
	    
	    try {
	    	// store last scan
	    	admin.storeConfigParameter(ConfigLastScanPerDay, end);
	    				
	    	persistMetrics.storePerformance("AggregateTimer-PerDay", start, Calendar.getInstance());
		} catch (Exception e) {
			LOG.warning("Error performance trace :"+e.getMessage());
		}
	}
    
}
