/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.middlewaresolutions.ofmconsole.jobs.aggregate;

import java.io.IOException;
import java.util.Calendar;

import javax.naming.NamingException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.jobs.AbstractJob;

/**
 * Scan JMX Server information
 * @author Emmanuel
 */
public class Aggregate5MinJob extends AbstractJob {
	
	private static String ConfigLastScanPer5Min = "ConfigLastScanPer5Min";
	
	/**
	 * Return last scan store in DB
	 * @throws NamingException 
	 */
	private Calendar getLastScanPer5Min() throws NamingException {		
		Calendar lastCalendar = admin.getConfigParameterDate(this.ConfigLastScanPer5Min);
		
		if (lastCalendar == null)
			lastCalendar = Calendar.getInstance();
		
		return lastCalendar;
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LOG.info("Aggregate Per 5 Min");

	    Calendar start = Calendar.getInstance();
	    Calendar end = null; 
	    
	    // aggregate last 5 min
	    try {
	    	persistMetrics.storePerformance("AggregateTimer-Per5Min", start, null);
	    	
			end = agg.aggregatePer5Min(getLastScanPer5Min());

		} catch (Exception e) {
			LOG.warning("Error during aggregation per 5min :"+e.getMessage());
		}
	    
	    try {
	    	// store last scan
	    	admin.storeConfigParameter(ConfigLastScanPer5Min, end);
	    				
	    	persistMetrics.storePerformance("AggregateTimer-Per5Min", start, Calendar.getInstance());
		} catch (Exception e) {
			LOG.warning("Error performance trace :"+e.getMessage());
		}
	}
    
}
