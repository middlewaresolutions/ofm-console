/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.middlewaresolutions.ofmconsole.jobs.aggregate;

import java.io.IOException;
import java.util.Calendar;

import javax.naming.NamingException;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.jobs.AbstractJob;

/**
 * Scan JMX Server information
 * @author Emmanuel
 */
public class Aggregate15MinJob extends AbstractJob {
	
	private static String ConfigLastScanPer15Min = "ConfigLastScanPer15Min";
	
	/**
	 * Return last scan store in DB
	 * @throws NamingException 
	 */
	private Calendar getLastScanPer15Min() throws NamingException {		
		Calendar lastCalendar = admin.getConfigParameterDate(this.ConfigLastScanPer15Min);
		
		if (lastCalendar == null)
			lastCalendar = Calendar.getInstance();
		
		return lastCalendar;
	}
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LOG.info("Aggregate Per 15 Min");

	    Calendar start = Calendar.getInstance();
	    Calendar end = null; 
	    
	    // aggregate last 5 min
	    try {
	    	persistMetrics.storePerformance("AggregateTimer-Per15Min", start, null);
	    	
			end = agg.aggregatePer15Min(getLastScanPer15Min());

		} catch (Exception e) {
			LOG.warning("Error during aggregation per 15min :"+e.getMessage());
		}
	    
	    try {
	    	// store last scan
	    	admin.storeConfigParameter(ConfigLastScanPer15Min, end);
	    				
	    	persistMetrics.storePerformance("AggregateTimer-Per15Min", start, Calendar.getInstance());
		} catch (Exception e) {
			LOG.warning("Error performance trace :"+e.getMessage());
		}
	}
    
}
