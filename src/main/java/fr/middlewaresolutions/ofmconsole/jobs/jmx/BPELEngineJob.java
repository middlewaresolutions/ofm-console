/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.middlewaresolutions.ofmconsole.jobs.jmx;

import java.io.IOException;
import java.util.Calendar;

import javax.naming.NamingException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.jobs.AbstractJob;

/**
 * Scan JMX Server information
 * @author Emmanuel
 */
public class BPELEngineJob extends AbstractJob {
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Calendar start = Calendar.getInstance();
		
	    LOG.info("Job: BpelEngine");
	    try {
			
			//Scan for all domains
 	    	for(Domain domain: admin.listDomainsToScan()) {
	    		try {			
					persistMetrics.saveBase(infra.listDomainBpelEngine(domain));
				} catch (IOException e) {
					LOG.warning("Error during BpelEngine scan of "+domain.getHostname()+": "+e.getMessage());
				}
	    	}
			
 	    	persistMetrics.storePerformance("ScanAndPersist-BpelEngine", start, Calendar.getInstance());
		} catch (Exception e) {
			LOG.warning("Error scan :"+e.getMessage());
		}
	}
    
}
