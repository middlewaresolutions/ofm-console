/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.middlewaresolutions.ofmconsole.jobs.jmx;

import java.io.IOException;
import java.util.Calendar;

import javax.ejb.EJB;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.middlewaresolutions.ofmconsole.administration.api.Administration;
import fr.middlewaresolutions.ofmconsole.administration.model.Domain;
import fr.middlewaresolutions.ofmconsole.api.Infrastructure;
import fr.middlewaresolutions.ofmconsole.api.PersistMetrics;
import fr.middlewaresolutions.ofmconsole.jobs.AbstractJob;

/**
 * Scan JMX Server information
 * @author Emmanuel
 */
public class ServerJob extends AbstractJob {
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Calendar start = Calendar.getInstance();
		
	    LOG.info("Job: Servers");
	    try {
			
			//Scan for all domains
 	    	for(Domain domain: admin.listDomainsToScan()) {
	    		try {
	    			persistMetrics.saveBase(infra.listDomainServers(domain));
				} catch (IOException e) {
					LOG.warning("Error during scan of "+domain.getHostname()+": "+e.getMessage());
				}
	    	}
			
 	    	persistMetrics.storePerformance("ScanAndPersist-Servers", start, Calendar.getInstance());
		} catch (Exception e) {
			LOG.warning("Error scan :"+e.getMessage());
		}
	}
    
}
