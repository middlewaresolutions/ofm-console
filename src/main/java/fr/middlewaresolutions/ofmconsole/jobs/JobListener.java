package fr.middlewaresolutions.ofmconsole.jobs;

import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import fr.middlewaresolutions.ofmconsole.api.JobsManager;

/**
 * WebApp listener for Jobs
 * 
 * @author Emmanuel
 *
 */
@WebListener
public class JobListener implements ServletContextListener {
	
	protected Logger LOG = Logger.getLogger(this.getClass().getName());
	
	public JobListener() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// stop jobs
		getJobManager().stop();
	}

	private JobsManager manager;

	protected JobsManager getJobManager() {
		if (manager == null)
			try {
				manager = (JobsManager) InitialContext.doLookup("java:module/JobsManagerBean");
			} catch (NamingException e) {
				try {
					manager = (JobsManager) InitialContext.doLookup("java:module/JobsManagerBean");
				} catch (NamingException e1) {
					LOG.warning(e.getMessage());
				}
			}
		
		return manager;
	}
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// start jobs
		getJobManager().start();
	}

}
