package fr.middlewaresolutions.ofmconsole.jobs.delete;

import java.util.Calendar;

import javax.naming.NamingException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.jobs.AbstractJob;

/**
 * Scan JMX Server information
 * @author Emmanuel
 */
public class DeleteMetricPerHourJob extends AbstractJob {
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Calendar start = Calendar.getInstance();
		
	    LOG.info("Job: Delete MetricPerHour");
	    try {
	    	//begin
	    	persistMetrics.storePerformance("Delete-MetricPerHour", start, null);
	    	
	    	String hourToKeep = admin.getConfigParameterValue("DeleteMetricPerHour-KeepHour", "-48");
	    	Calendar end = Calendar.getInstance();
	    	end.add(Calendar.HOUR, Integer.valueOf(hourToKeep));
	    	
	    	String hourToStart = admin.getConfigParameterValue("DeleteMetricPerHour-StartHour", "-96");
	    	Calendar begin = Calendar.getInstance();
	    	end.add(Calendar.HOUR, Integer.valueOf(hourToStart));
	    	
			clean.cleanMetricPerHour(begin, end);
			
			//end
			persistMetrics.storePerformance("Delete-MetricPerHour", start, Calendar.getInstance());
		} catch (CockpitException e) {
			LOG.warning("Error scan :"+e.getMessage());
		}
	}
    
}
