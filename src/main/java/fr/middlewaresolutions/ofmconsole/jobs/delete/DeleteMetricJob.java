/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.middlewaresolutions.ofmconsole.jobs.delete;

import java.util.Calendar;

import javax.naming.NamingException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.jobs.AbstractJob;

/**
 * Scan JMX Server information
 * @author Emmanuel
 */
public class DeleteMetricJob extends AbstractJob {
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Calendar start = Calendar.getInstance();
		
	    LOG.info("Job: Delete Metric");
	    try {
	    	//begin
	    	persistMetrics.storePerformance("Delete-Metric", start, null);
	    	
	    	String hourToKeep = admin.getConfigParameterValue("DeleteMetric-KeepHour", "-12");
	    	Calendar end = Calendar.getInstance();
	    	end.add(Calendar.HOUR, Integer.valueOf(hourToKeep));
	    	
	    	String hourToStart = admin.getConfigParameterValue("DeleteMetric-StartHour", "-36");
	    	Calendar begin = Calendar.getInstance();
	    	end.add(Calendar.HOUR, Integer.valueOf(hourToStart));
	    	
			clean.cleanMetric(begin, end);
			
			//end
			persistMetrics.storePerformance("Delete-Metric", start, Calendar.getInstance());
		} catch (CockpitException e) {
			LOG.warning("Error scan :"+e.getMessage());
		}
	}
    
}
