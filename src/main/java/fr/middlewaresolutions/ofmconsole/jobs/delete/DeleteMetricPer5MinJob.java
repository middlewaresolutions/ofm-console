/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.middlewaresolutions.ofmconsole.jobs.delete;

import java.util.Calendar;

import javax.naming.NamingException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import fr.middlewaresolutions.ofmconsole.exception.CockpitException;
import fr.middlewaresolutions.ofmconsole.jobs.AbstractJob;

/**
 * Scan JMX Server information
 * @author Emmanuel
 */
public class DeleteMetricPer5MinJob extends AbstractJob {
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Calendar start = Calendar.getInstance();
		
	    LOG.info("Job: Delete MetricPer5Min");
	    try {
	    	//begin
	    	persistMetrics.storePerformance("Delete-MetricPer5Min", start, null);
	    	
	    	String hourToKeep = admin.getConfigParameterValue("DeleteMetricPer5Min-KeepHour", "-48");
	    	Calendar end = Calendar.getInstance();
	    	end.add(Calendar.HOUR, Integer.valueOf(hourToKeep));
	    	
	    	String hourToStart = admin.getConfigParameterValue("DeleteMetricPer5Min-StartHour", "-96");
	    	Calendar begin = Calendar.getInstance();
	    	end.add(Calendar.HOUR, Integer.valueOf(hourToStart));
	    	
			clean.cleanMetricPer5Min(begin, end);
			
			//end
			persistMetrics.storePerformance("Delete-MetricPer5Min", start, Calendar.getInstance());
		} catch (CockpitException e) {
			LOG.warning("Error scan :"+e.getMessage());
		}
	}
    
}
