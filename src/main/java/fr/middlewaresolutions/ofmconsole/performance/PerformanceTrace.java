/**
 * 
 */
package fr.middlewaresolutions.ofmconsole.performance;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Performance trace
 * @author Emmanuel
 *
 */
@Entity
public class PerformanceTrace {

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start_trace")
	private Calendar start;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_trace")
	private Calendar end;
	
	@Id
	private String name;
	
	/**
	 * 
	 */
	public PerformanceTrace() {
		// TODO Auto-generated constructor stub
	}
	
	public PerformanceTrace(String name, Calendar start, Calendar end) {
		this.name = name;
		this.start = start;
		this.end = end;
	}

	/**
	 * @return the start
	 */
	public Calendar getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(Calendar start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public Calendar getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(Calendar end) {
		this.end = end;
	}

}
