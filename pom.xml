<?xml version="1.0" encoding="UTF-8"?>
<!--
    JBoss, Home of Professional Open Source
    Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
    contributors by the @authors tag. See the copyright.txt in the
    distribution for a full listing of individual contributors.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>fr.middlewaresolutions.ofmcockpit</groupId>
    <artifactId>ofm-cockpit</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>war</packaging>
    <name>Oracle Fusion Middleware Cockipt</name>
    <description>A cockpit for Weblogic Oracle Fusion Middleware</description>


    <properties>
        <!-- Explicitly declaring the source encoding eliminates the following 
            message: -->
        <!-- [WARNING] Using platform encoding (UTF-8 actually) to copy filtered 
            resources, i.e. build is platform dependent! -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- JBoss dependency versions -->
        <version.wildfly.maven.plugin>1.0.2.Final</version.wildfly.maven.plugin>

        <!-- Define the version of the JBoss BOMs we want to import to specify 
            tested stacks. -->
        <version.jboss.bom>8.2.1.Final</version.jboss.bom>

        <!-- other plugin versions -->
        <version.compiler.plugin>3.1</version.compiler.plugin>
        <version.surefire.plugin>2.16</version.surefire.plugin>
        <version.war.plugin>2.5</version.war.plugin>

        <!-- maven-compiler-plugin -->
        <maven.compiler.target>1.7</maven.compiler.target>
        <maven.compiler.source>1.7</maven.compiler.source>
        
        <version.eclipselink>2.6.3</version.eclipselink>
        <version.weblogic>12.1.3-0-0</version.weblogic>
    </properties>

	<repositories>
		<repository>
			<id>prime-repo</id>
			<name>PrimeFaces Maven Repository</name>
			<url>http://repository.primefaces.org</url>
			<layout>default</layout>
		</repository>
	</repositories>
	
    <dependencyManagement>
        <dependencies>
            <!-- JBoss distributes a complete set of Java EE 7 APIs including a Bill
                of Materials (BOM). A BOM specifies the versions of a "stack" (or a collection) 
                of artifacts. We use this here so that we always get the correct versions 
                of artifacts. Here we use the jboss-javaee-7.0-with-tools stack (you can
                read this as the JBoss stack of the Java EE 7 APIs, with some extras tools
                for your project, such as Arquillian for testing) and the jboss-javaee-7.0-with-hibernate
                stack you can read this as the JBoss stack of the Java EE 7 APIs, with extras
                from the Hibernate family of projects)
            <dependency>
                <groupId>org.wildfly.bom</groupId>
                <artifactId>jboss-javaee-7.0-with-tools</artifactId>
                <version>${version.jboss.bom}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            -->
            
            <dependency>
			  <groupId>org.jboss.arquillian</groupId>
			  <artifactId>arquillian-bom</artifactId>
			  <version>1.1.11.Final</version>
			  <type>pom</type>
			  <scope>import</scope>
			</dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>

        <!-- First declare the APIs we depend on and need for compilation. All 
        of them are provided by JBoss WildFly -->

        <!-- Import the CDI API, we use provided scope as the API is included in 
        JBoss WildFly -->
        <dependency>
            <groupId>javax.enterprise</groupId>
            <artifactId>cdi-api</artifactId>
            <version>1.2</version>
            <scope>provided</scope>
        </dependency>

        <!-- https://mvnrepository.com/artifact/javax/javaee-api -->
        <dependency>  
            <groupId>org.jboss.spec</groupId>  
            <artifactId>jboss-javaee-6.0</artifactId>  
            <version>1.0.0.Final</version>  
            <type>pom</type>  
            <scope>provided</scope>  
        </dependency>  

        <dependency>
            <groupId>org.primefaces</groupId>
            <artifactId>primefaces</artifactId>
            <version>6.0</version>
        </dependency>
		
        <dependency>  
            <groupId>org.primefaces.themes</groupId>  
            <artifactId>all-themes</artifactId>  
            <version>1.0.10</version>  
        </dependency>

		<!-- https://mvnrepository.com/artifact/org.eclipse.persistence/javax.persistence -->
		<dependency>
		    <groupId>org.eclipse.persistence</groupId>
		    <artifactId>javax.persistence</artifactId>
		    <version>2.1.1</version>
		</dependency>
		
        <!-- https://mvnrepository.com/artifact/org.eclipse.persistence/eclipselink -->
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>eclipselink</artifactId>
            <version>${version.eclipselink}</version>
        </dependency>
		
        <!-- https://mvnrepository.com/artifact/org.eclipse.persistence/org.eclipse.persistence.jpars -->
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>org.eclipse.persistence.jpars</artifactId>
            <version>${version.eclipselink}</version>
        </dependency>
		
        <!-- https://mvnrepository.com/artifact/org.eclipse.persistence/org.eclipse.persistence.moxy -->
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>org.eclipse.persistence.moxy</artifactId>
            <version>${version.eclipselink}</version>
        </dependency>
		
		<!-- https://mvnrepository.com/artifact/org.eclipse.persistence/org.eclipse.persistence.nosql -->
		<dependency>
		    <groupId>org.eclipse.persistence</groupId>
		    <artifactId>org.eclipse.persistence.nosql</artifactId>
		    <version>${version.eclipselink}</version>
		</dependency>
		
		
        <!-- For mathematics calculates -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-math3</artifactId>
            <version>3.6.1</version>
        </dependency>
		
		<!-- Cron Scheduling -->
		<dependency>
			<groupId>org.quartz-scheduler</groupId>
			<artifactId>quartz</artifactId>
			<version>2.2.3</version>
		</dependency>
		<dependency>
			<groupId>org.quartz-scheduler</groupId>
			<artifactId>quartz-jobs</artifactId>
			<version>2.2.3</version>
		</dependency> 
	  
        <!-- Needed for running tests (you may also use TestNG) -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.jboss.arquillian.junit</groupId>
            <artifactId>arquillian-junit-container</artifactId>
            <scope>test</scope>
        </dependency>        
		
		
        <dependency>
            <groupId>org.jboss.shrinkwrap.resolver</groupId>
            <artifactId>shrinkwrap-resolver-depchain</artifactId>
            <scope>test</scope>
            <type>pom</type>
        </dependency>


        <dependency>
        	<groupId>com.oracle.weblogic</groupId>
        	<artifactId>wljmxclient</artifactId>
        	<version>12.1.3</version>
        	<scope>system</scope>
        	<systemPath>${project.basedir}/lib/wljmxclient-12.1.3.jar</systemPath>
        </dependency>
        
        <dependency>
        	<groupId>com.oracle.weblogic</groupId>
        	<artifactId>wlthint3clientclient</artifactId>
        	<version>12.1.3</version>
        	<scope>system</scope>
        	<systemPath>${project.basedir}/lib/wlthint3client-12.1.3.jar</systemPath>
        </dependency>
    </dependencies>

    <build>
        <!-- Maven will append the version to the finalName (which is the name 
            given to the generated war, and hence the context root) -->
        <finalName>${project.artifactId}</finalName>
        <plugins>
            <plugin>
                <artifactId>maven-war-plugin</artifactId>
                <version>${version.war.plugin}</version>
                <configuration>
                    <!-- Java EE 7 doesn't require web.xml, Maven needs to catch up! -->
                    <failOnMissingWebXml>false</failOnMissingWebXml>
                    <warName>${project.artifactId}</warName>
                </configuration>
            </plugin>
            
            
        </plugins>
    </build>

    <profiles>
        <profile>
            <!-- Just to debug without execute SureFire tests -->
            <id>netbeans</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>${version.surefire.plugin}</version>
                        <configuration>
                            <skip>true</skip>                    
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        
        <profile>
            <!-- The default profile skips all tests, though you can tune it to run 
            just unit tests based on a custom pattern -->
            <!-- Seperate profiles are provided for running all tests, including Arquillian 
            tests that execute in the specified container -->
            <id>arquillian</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <artifactId>maven-surefire-plugin</artifactId>
                        <version>${version.surefire.plugin}</version>
                        <configuration>
                            <skip>false</skip>
                            <includes>
                                <include>**/*DerbyTest.java</include>
                            </includes>
                        </configuration>
                    </plugin>
                    
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <version>2.19.1</version>
                        <configuration>
                            <includes>
                                <include>**/*MySQLTest.java</include>
                                <include>**/*OracleTest.java</include>
                            </includes>
                        </configuration>
                    </plugin>
                    
                    
                </plugins>
            </build>
        </profile>

        <profile>
            <!-- The default profile skips all tests, though you can tune it to run 
            just unit tests based on a custom pattern -->
            <!-- Seperate profiles are provided for running all tests, including Arquillian 
            tests that execute in the specified container -->
            <id>weblogic-CompactDomain</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <wls.hostname>localhost</wls.hostname>
                <wls.port>7001</wls.port>
                <wls.user>weblogic</wls.user>
                <wls.password>welcome1</wls.password>
                <wls.targets>AdminServer</wls.targets>
            </properties>
            
            <dependencies>
                <dependency>
                    <groupId>org.jboss.arquillian.container</groupId>
                    <artifactId>arquillian-wls-remote-12.1.x</artifactId>
                    <version>1.0.1.Final</version>
                    <scope>test</scope>
                </dependency>
            </dependencies>
            
            <build>
                <plugins>
                    <!-- Deploy DataSource on Weblogic -->
                    <plugin>
                        <groupId>com.oracle.weblogic</groupId>
                        <artifactId>weblogic-maven-plugin</artifactId>
                        <version>${version.weblogic}</version>
                        <executions>
                            <execution>
                                <phase>pre-integration-test</phase>
                                <goals>
                                    <goal>deploy</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <adminurl>t3://${wls.hostname}:${wls.port}</adminurl>
                            <user>${wls.user}</user>
                            <password>${wls.password}</password>
                            <upload>true</upload>
                            <action>deploy</action>
                            <targets>${wls.targets}</targets>
                            <remote>true</remote>
                            <verbose>true</verbose>
                            <debug>true</debug>
                            <source>${project.basedir}/src/test/weblogic/CockpitMemoryDS-jdbc.xml</source>
                            <source>${project.basedir}/src/test/weblogic/CockpitMySQLDS-jdbc.xml</source>
                        </configuration>
                    </plugin>
                    
                    <plugin>
	                  <groupId>org.apache.maven.plugins</groupId>
	                  <artifactId>maven-surefire-plugin</artifactId>
	                  <configuration>
	                     <systemPropertyVariables>
	                        <arquillian.launch>weblogic</arquillian.launch>
	                     </systemPropertyVariables>
	                  </configuration>
	               </plugin>
                </plugins>
            </build>
        </profile>
       
        <profile>
            <!-- The default profile skips all tests, though you can tune it to run 
            just unit tests based on a custom pattern -->
            <!-- Seperate profiles are provided for running all tests, including Arquillian 
            tests that execute in the specified container -->
            <id>Wildfly</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <properties>
                <wf.hostname>localhost</wf.hostname>
                <wf.port>7001</wf.port>
                <wf.user>weblogic</wf.user>
                <wf.password>welcome1</wf.password>
            </properties>
            
            <dependencies>
                <dependency>
                    <groupId>org.wildfly.arquillian</groupId>
                    <artifactId>wildfly-arquillian-container-remote</artifactId>
                    <version>1.1.0.Final</version>
                    <scope>test</scope>
                </dependency>
            </dependencies>
        </profile>
    </profiles>
</project>
